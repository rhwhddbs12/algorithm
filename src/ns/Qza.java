package ns;

import java.util.Scanner;

public class Qza {
    static String s;
    static int res=0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        s = scanner.nextLine();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'z' || s.charAt(i) == 'a') {
                char al= s.charAt(i);
                sol(i+1,al);
            }
        }
        System.out.println(res);
    }

    public static void sol(int x,char al){

        for (int i = x; i < s.length(); i++) {
            if (s.charAt(i) == 'z' || s.charAt(i) == 'a') {
                if (al != s.charAt(i)) {
                    res++;
                }
                return;
            }
        }
    }
}
