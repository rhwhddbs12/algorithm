package ns;

import java.util.Arrays;

public class Qbishop {
    static int [][] d=new int[8][8];
    public static void main(String[] args) {
        int N =3;
        String[] bishops = new String[N];
        bishops[0]="C6";bishops[1]="A4";bishops[2]="E5";
        for (int i = 0; i < bishops.length; i++) {
            int y= (int)bishops[i].charAt(0) -65;
            int x =(int)bishops[i].charAt(1)-49;
            sol(y,x);
        }
        int cnt=0;
        for (int i = 0; i < 8; i++) {
            System.out.print("[");
            for (int j = 0; j < 8; j++) {
                if(d[i][j]==0)cnt++;
                System.out.print(d[i][j]+" ");
            }
            System.out.println("]");
        }
        System.out.println(cnt);
    }
    public static void sol(int y,int x) {

        d[y][x]=1;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (Math.abs(y - i) == Math.abs(x - j)) {
                    d[i][j]=1;
                }
            }
        }
    }
}
