package practice;

import java.util.ArrayList;
import java.util.Scanner;

public class arryStack {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] arr = new int[N];
        ArrayList<Integer> output = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            arr[i] = scanner.nextInt();
            if(i!=0 && arr[i-1]!=arr[i]){
                output.add(arr[i]);
            }else if(i==0){
                output.add(arr[i]);
            }
        }
        
        System.out.println(output);

    }
}
