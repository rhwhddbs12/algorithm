package backJoon.recursion;

import java.util.Scanner;

//별 찍기
public class Q2447 {
    static String[][] ary;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N= scanner.nextInt();
        ary = new String[N][N];
        int N2=N;
        recursion(N,N2);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                sb.append(ary[i][j]);
            }
            sb.append("\n");
        }
        System.out.println(sb);
    }

    public static void recursion(int N, int N2){
        //종료 조건
        if (N / 3 != 1) {
            recursion(N/3,N2);
        }
        //실행 조건
        for (int i = 0; i < N2; i++) {
            for (int j = 0; j < N2; j++) {
                if (i%N > N/3-1 && j%N > N/3-1 && j%N< (N/3)*2 && i%N< (N/3)*2 ) {
                    ary[i][j]=" ";
                }else{
                    if(ary[i][j]==" ") continue;
                    ary[i][j]="*";
                }

            }
        }
    }

}
