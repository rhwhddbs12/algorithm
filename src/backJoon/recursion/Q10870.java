package backJoon.recursion;

import java.util.Scanner;

public class Q10870 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        System.out.println(fibo(N));
    }
    public static int fibo(int N){
        if(N>1){
            return fibo(N-1) + fibo(N - 2);
        }
        else{
            return N;
        }
    }
}
