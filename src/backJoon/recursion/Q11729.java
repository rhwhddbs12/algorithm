package backJoon.recursion;

import java.util.Scanner;

/**
 * 장대는 총 세개와 N개의 원판이 주어짐 (1<=N<=20)
 * 첫째줄에 옮긴횟수 K
 * 둘쨰줄부터 옮긴 과정 입력
 */
//하노이의 탑 이동 순서
public class Q11729 {
    static int N; //원판의 갯수
    static StringBuffer sb = new StringBuffer();
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();

        sb.append((int)Math.pow(2,N)-1 + "\n");
        hanoi(N,1,2,3);
        System.out.println(sb);
    }

    public static void hanoi(int N, int start, int mid, int to) {
        // 이동할 원반의 수가 1개라면?
        if (N == 1) {
            sb.append(start + " " + to + "\n");
            return;
        }
        // STEP 1 : N-1개를 A에서 B로 이동
        hanoi(N - 1, start, to, mid);

        // STEP 2 : 1개를 A에서 C로 이동
        sb.append(start + " " + to + "\n");

        // STEP 3 : N-1개를 B에서 C로 이동
        hanoi(N - 1, mid, start, to);

    }
}
