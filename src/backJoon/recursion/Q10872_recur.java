package backJoon.recursion;

import java.util.Scanner;

public class Q10872_recur {
    static int sum = 1;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();

        System.out.println(recur(N));
    }
    public static int recur(int N){
        if (N > 0) {
            sum *= N;
            N--;
            return recur(N);
        }
        return sum;
    }
}
