package backJoon.recursion;

import java.util.Scanner;

public class Q10872 {
    static int i = 1;
    static int sum = 1;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();

        System.out.println(recur(N));
    }
    public static int recur(int N){
        while(i<=N){
            sum *= i;
            i++;
        }
        return sum;
    }
}
