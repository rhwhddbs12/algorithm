package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;
//토마토
public class Q7576 {

    static int N; // 세로 y
    static int M; // 가로 x
    static int[][] map;
    static int[][] visited;

    /**
     * 1 = 익은 토마토
     * 0 = 안익은 토마토
     * -1 = 토마토 없음
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        M = scanner.nextInt();
        N = scanner.nextInt();
        map = new int[N][M];
        visited = new int[N][M];
        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                map[i][j]= scanner.nextInt();
                visited[i][j]=map[i][j];
                if (map[i][j] == 1) {
                    qx.add(j);qy.add(i);
                }
            }
        }

        bfs(map, visited, qx, qy);

        int res =0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (visited[i][j] == 0) {
                    System.out.println("-1");
                    return;
                }else{
                    if (res < visited[i][j]) {
                        res = visited[i][j];
                    }
                }

            }
        }

        System.out.println(res-1);
    }

    //시작점은 하나 뿐만이 아니다.

    public static void bfs(int[][] map, int[][] visited, Deque<Integer> qx, Deque<Integer> qy) {
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();
            int v = visited[y][x];

                //상
                if (y - 1 > -1 && visited[y-1][x]==0 && map[y-1][x]==0) {
                    qy.add(y - 1);qx.add(x);visited[y-1][x] =v+1;
                }
                //하
                if (y+1<N  && visited[y+1][x]==0 && map[y+1][x]==0) {
                  qy.add(y + 1);qx.add(x);visited[y+1][x] =v+1;
                }
                //좌
                if (x - 1 > -1 && visited[y][x-1]==0 && map[y][x-1]==0) {
                    qy.add(y);qx.add(x-1);visited[y][x-1] =v+1;
                }
                //우
                if (x + 1 < M && visited[y][x+1]==0 && map[y][x+1]==0) {
                    qy.add(y);qx.add(x+1);visited[y][x+1] =v+1;
                }
        }
    }

}
