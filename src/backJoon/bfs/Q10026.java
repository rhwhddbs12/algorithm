package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

//적록색약
public class Q10026 {
    static int N;
    static int section=0;
    static int section2=0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        scanner.nextLine();
        char[][] map = new char[N][N];
        char[][] map2 = new char[N][N];
        int[][] visited = new int[N][N];
        char color;

        for (int i = 0; i < N; i++) {
            String str = scanner.nextLine();
            for (int j = 0; j < N; j++) {
                map[i][j] = str.charAt(j);
                map2[i][j] = str.charAt(j);
            }
        }
        //R와 G를 하나로 본다.
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (map2[i][j] == 'R') {
                    map2[i][j] = 'G';
                }
            }
        }
        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();

        //일반 사람이 봤을때 구역
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (visited[i][j] == 0) {
                    qx.add(j);qy.add(i);
                    color = map[i][j];
                    bfs(map,visited,qx,qy,color);
                    section+=1;
                }
            }
        }
        visited = new int[N][N]; // 방문했을때 기록 초기화
        //적록 색약이 봤을때 구역
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (visited[i][j] == 0) {
                    qx.add(j);qy.add(i);
                    color = map2[i][j];
                    bfs(map2,visited,qx,qy,color);
                    section2+=1;
                }
            }
        }

        System.out.println(section+" "+section2);
    }
    public static void bfs(char[][] map, int[][] visited, Deque<Integer> qx,Deque<Integer> qy,char color){
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();

            //상
            if (y - 1 > -1 && visited[y - 1][x] == 0 && map[y - 1][x] == color) {
                qy.add(y - 1);qx.add(x);visited[y - 1][x]=1;
            }
            //하
            if (y + 1 < N && visited[y + 1][x] == 0 && map[y + 1][x] == color) {
                qy.add(y + 1);qx.add(x);visited[y + 1][x]=1;
            }
            //좌
            if (x - 1 > -1 && visited[y][x - 1] == 0 && map[y][x - 1] == color) {
                qy.add(y);qx.add(x - 1);visited[y][x - 1]=1;
            }
            //우
            if (x + 1 < N && visited[y][x + 1] == 0 && map[y][x + 1] == color) {
                qy.add(y);qx.add(x + 1);visited[y][x + 1]=1;
            }
        }
    }
}
