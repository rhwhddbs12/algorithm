package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;
//연구소

/**
 * 0: 빈칸, 1: 벽, 2: 바이러스
 */
public class Q14502 {
    static int N; //세로
    static int M; //가로
    static int cnt=0;
    static int res=0;

    static Deque<Integer> qx = new ArrayDeque<>();
    static Deque<Integer> qy = new ArrayDeque<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        M = scanner.nextInt();
        int[][] map = new int[N][M];
        int[][] map2 = new int[N][M];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                map[i][j] = scanner.nextInt();
                map2[i][j] = map[i][j];    // 배열 복사는 일일이 해줘여함 map2=map 이런식으로 안됨 !
            }
        }

        // 벽 3개를 고른다.
        dfs(map,0);
        System.out.println(res);

    }

    //바이러스 퍼짐
    public static void bfs(int[][] map,int[][] visited, Deque<Integer> qx, Deque<Integer> qy) {

        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();

            // visited에서 바이러스 감염은 =1 감염 x = 0 벽 =1
            //상
            if (y - 1 > -1 && map[y - 1][x] == 0 && visited[y - 1][x] == 0) {
                qy.add(y-1);qx.add(x);visited[y-1][x]=1; map[y-1][x]=2;
            }
            //하
            if (y + 1 < N && map[y + 1][x] == 0 && visited[y + 1][x] == 0) {
                qy.add(y+1);qx.add(x);visited[y+1][x]=1; map[y+1][x]=2;
            }
            //좌
            if (x - 1 > -1 && map[y][x-1] == 0 && visited[y][x-1] == 0) {
                qy.add(y);qx.add(x-1);visited[y][x-1]=1; map[y][x-1]=2;
            }
            //우
            if (x +1 < M && map[y][x+1] == 0 && visited[y][x+1] == 0) {
                qy.add(y);qx.add(x+1);visited[y][x+1]=1; map[y][x+1]=2;
            }
        }
        //0의 개수 카운트 정답 최대값 찾기
        count(map);
        if (res < cnt) {
            res=cnt;
        }
    }
    //벽을 세움
    public static void dfs(int[][] map, int depth){
        if (depth == 3) {
            int[][] copyMap = new int[N][M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    if (map[i][j] == 2) {
                        qx.add(j);
                        qy.add(i);
                    }
                    copyMap[i][j] = map[i][j];
                }
            }
            int[][] visited = new int[N][M];
            bfs(copyMap,visited,qx,qy);
            return;
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (map[i][j] == 0 ) {
                    map[i][j]=1;
                    dfs(map, depth + 1);
                    map[i][j]=0;

                }
            }
        }
    }
    //감염되지 않은 바이러스 카운트
    public static int count(int[][] map){
        cnt=0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (map[i][j] == 0) {
                    cnt+=1;
                }
            }
        }
        return cnt;
    }
}
