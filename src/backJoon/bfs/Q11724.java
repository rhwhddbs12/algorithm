package backJoon.bfs;

import java.util.*;
//연결 요소의 개수 
public class Q11724 {
        static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        int M = scanner.nextInt();
        int cnt =0;
        int[][] map = new int[N][N];
        int[][] visited = new int[N][N];
        ArrayList<Integer> check = new ArrayList<>();
        int[] c = new int[N];

        for (int i = 0; i < M; i++) {

            int f = scanner.nextInt();
            int t = scanner.nextInt();
            map[f - 1][t - 1] = 1;
            map[t - 1][f - 1] = 1;
            c[f-1]=1;
            c[t-1]=1;
        }
        for (int i = 0; i < N; i++){
            if (c[i] == 0) {
                cnt += 1;
            }
        }
//        System.out.println("cnt = " + cnt);
//        for (int i = 0; i < N; i++) {
//            System.out.print("[");
//            for (int j = 0; j < N; j++) {
//                System.out.print(map[i][j]+", ");
//            }
//            System.out.println("]");
//        }

        Deque<Integer> qs = new ArrayDeque<>();


        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (map[i][j] == 1 && visited[i][j] == 0 &&visited[j][i] == 0) {
                    qs.add(i);
                    bfs(map, visited, qs);
                    cnt += 1;
                }
            }
        }
        System.out.println(cnt);
    }

    public static void bfs(int[][] map, int[][] visited, Deque<Integer> qs) {

        while (!qs.isEmpty()) {

            int s = qs.pop();

            for (int i = 0; i < N; i++) {
                if (map[s][i] == 1 && visited[s][i]==0) {
                    qs.add(i); visited[s][i]=1;
                }
            }

        }
    }


}
