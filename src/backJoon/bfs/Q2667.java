package backJoon.bfs;

import java.util.*;
// 단지번호 붙이기
public class Q2667 {
    static int N;  //크기 N x N
    static int[][] map;  // 정사각형 안의 집들 위치
    static int[][] visited; // 방문여부
    static int hsum=0; // 단지 총 합
    static ArrayList<Integer> res = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N= scanner.nextInt();
        map=new int[N][N];
        visited=new int[N][N];
        scanner.nextLine();
        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();


        // input값이 int가 아닌 문자열이므로 문자열-> int로 int배열안에 숫자 넣기
        for (int i = 0; i < N; i++) {
            String str = scanner.nextLine();
            for (int j = 0; j < N; j++) {
                map[i][j] = str.charAt(j) - '0';
            }
        }
        //visited와 queue를 이용해서 단지갯수와 bfs()내에서 집 갯수 구하기
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (map[i][j] == 1 && visited[i][j]==0) {
                    qy.add(i);qx.add(j);
                    bfs(map,visited,qx,qy);
                    hsum+=1;
                }
            }
        }
        System.out.println(hsum); // 단지갯수

        Collections.sort(res); // 문제에서 오름차순 정렬하라고 했으니 ArraysList 정렬
        //답:
        while (!res.isEmpty()) {
            System.out.println(res.get(0));
            res.remove(0);
        }


    }

    public static void bfs(int[][] map, int[][] visited, Deque<Integer>qx, Deque<Integer> qy){
        int cnt=0;
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();

            //상
            if (y - 1 > -1 && visited[y - 1][x] == 0 && map[y - 1][x] == 1) {
                qy.add(y - 1);qx.add(x);visited[y-1][x]=1; cnt+=1;
            }
            //하
            if (y + 1 < N && visited[y + 1][x] == 0 && map[y + 1][x] == 1) {
                qy.add(y + 1);qx.add(x);visited[y+1][x]=1;cnt+=1;
            }
            //좌
            if (x - 1 > -1 && visited[y][x - 1] == 0 && map[y][x - 1] == 1) {
                qy.add(y);qx.add(x - 1);visited[y][x-1]=1;cnt+=1;
            }
            //우
            if (x + 1 < N && visited[y][x + 1] == 0 && map[y][x + 1] == 1) {
                qy.add(y);qx.add(x + 1);visited[y][x+1]=1;cnt+=1;
            }

            if(cnt==0) cnt=1;
        }
        res.add(cnt);
    }
}
