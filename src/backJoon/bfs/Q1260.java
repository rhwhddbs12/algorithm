package backJoon.bfs;

import java.util.*;

//DFS와 BFS
public class  Q1260 {

    static int N;//정점의 갯수
    static int M;//간선의 갯수
    static int V;//시작정점 번호

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        N = scanner.nextInt();
        M = scanner.nextInt();
        V = scanner.nextInt();
        boolean[][] map = new boolean[N][N]; // 정점들사이 연결된 간선 정보
        int[][] visited = new int[N][N]; // 재방문 하지 않기위해 확인해줄 배열

        for (int i = 0; i <M; i++) {
            int f = scanner.nextInt(); // from
            int t = scanner.nextInt(); // to
            map[f-1][t-1] = true;
            map[t-1][f-1] = true;
        }
        //dfs
        dfs(map,visited,V);
        System.out.println();

        visited=new int[N][N];// visited 초기화

        //bfs
        Deque<Integer> que = new ArrayDeque<>();
        que.add(V);
        bfs(map, visited, que);
    }

    public static void bfs(boolean[][] map, int[][] visited, Deque<Integer> que) {
        while (!que.isEmpty()) {
            Integer f = que.pop(); // 시작정점
            System.out.print(f); // 큐에 들어온 순서대로 출력 -> 그래프 bfs로 방문 순서 출력

            //방향성이 없기 때문에 2차원 배열을 대칭으로 만들어줌
            for (int i = 0; i < map.length; i++) {
                visited[i][f - 1] = 1;
            }
            for (int i = 0; i < map.length; i++) {
                if (map[f-1][i] == true && visited[f-1][i] == 0) {
                    if(!que.contains(i+1)) que.add(i+1);
                }
            }

        }
    }

    public static void dfs(boolean[][] map, int[][] visited, int f) {
        System.out.print(f);
        //방향성이 없기 때문에 2차원 배열을 대칭으로 만들어줌
        for (int i = 0; i < map.length; i++) {
            visited[i][f - 1] = 1;
        }
        for (int i = 0; i < map.length; i++) {
            if (visited[f - 1][i] == 0 && map[f - 1][i] == true) {
                dfs(map, visited, i + 1);
            }
        }
    }

}
