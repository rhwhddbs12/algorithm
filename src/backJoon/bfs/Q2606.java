package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

public class Q2606 {
    static int N; //컴퓨터 수
    static int M; //연결된 네티워크 수
    static int[][] map ;
    static int[][] visited;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N= scanner.nextInt();
        M= scanner.nextInt();
        scanner.nextLine();
        map = new int[N][N];
        visited = new int[N][N];

        for (int i = 0; i < M; i++) {
            int fr = scanner.nextInt();
            int to = scanner.nextInt();
            map[fr-1][to-1]=1;
            map[to-1][fr-1]=1;
        }

        Deque<Integer> q = new ArrayDeque<>();
        q.add(0);
        bfs(map,q,visited);
    }

    public static void bfs(int[][] map, Deque<Integer> q, int[][] visited) {
        int temp = 0;
        while (!q.isEmpty()) {
            int fr = q.pop();
            for (int t = 0; t < N; t++) {
                if (map[fr][t] == 1 && visited[fr][t] == 0) {
                    q.add(t);
                    visited[fr][t] = 1;
                    visited[t][fr] = 1;
                    for (int i = 0; i < N; i++) {
                        visited[i][t] = 1;
                    }
                    temp++;
                }
            }
        }
        System.out.println("temp = " + temp);
    }
}
