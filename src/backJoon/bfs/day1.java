package backJoon.bfs;

import java.util.*;

//백준 알고리즘 1260: DFS와 BFS
//첫째 줄에 정점의 개수: N(1 ≤ N ≤ 1,000), 간선의 개수: M(1 ≤ M ≤ 10,000), 탐색을 시작할 정점의 번호: V가 주어진다.
public class day1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); //정점의 개수
        int M = scanner.nextInt(); //간선의 개수
        int V = scanner.nextInt(); //시작 정점 번호

        int f ; //from
        int t ; //to

        int[][] map = new int[N][N]; //갈 수 있는지 여부
        int[][] visited = new int[N][N]; //방문 했는지 여부 map과 동일    * dfs에서 방문한 점을 체크할때 주로 많이 쓰임
        // 두 정점사이 간선이 존재하면 1 아니면 0
        for (int i = 0; i < M; i++) {
            f= scanner.nextInt();
            t = scanner.nextInt();
            map[f-1][t-1]=1; // 배열의 인덱스는 0부터 시작하니까
            map[t-1][f-1]=1;
        }
        //깊이우선탐색
        dfs(map,visited,V);
        System.out.println();
        visited = new int[N][N];

        //너비우선탐색
        Deque<Integer> que = new ArrayDeque<>();
        que.add(V);
        bfs(map,visited,que);


    }

    public static void dfs(int [][] map, int[][] visited ,int f){
        System.out.print(f);
        for (int i = 0; i < map.length; i++) {
            visited[i][f-1]=1;
        }
        for (int i = 0; i < map.length; i++) {
            if (map[f-1][i] == 1 && visited[f-1][i] == 0) {
                dfs(map,visited,i+1);
            }

        }

    }
    public static void bfs(int [][] map, int[][] visited ,Deque<Integer> que){
        while (que.size() > 0) {
            Integer f = que.pop();
            System.out.print(f);
            //ex) 1->2 체크했는데 2->1 은이미 방문했다고 체크해주기 위해??
            for (int i = 0; i < map.length; i++) {
                visited[i][f - 1] = 1;
            }
            for (int to = 0; to < map.length; to++) {
                if (map[f - 1][to] == 1 && visited[f - 1][to] == 0) {
                    if (!que.contains(to+1)) {
                        que.add(to + 1);
                    }

                }
            }
        }

    }
}

//map: 점과점사이 연결상태를 나타내는 2차원 배열
//visited: 방문여부를 나타내줄 2차원 배열  (*map과 사이즈가 같음)

//dfs(깊이우선 탐색): map,visited 재귀 호출방식
//bfs(너비우선 탐색): map,visited,queue를 활용하여 while(que.size()>0) que.add형식으로 사용

//** if(!que.contains(데이터))를 사용하여 중복여부 체크