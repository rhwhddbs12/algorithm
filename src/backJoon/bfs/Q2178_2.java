package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

public class Q2178_2 {
    static int N;
    static int M;
    static int[][] map; // 경로 그래프
    static int[][] visited; // 방문 여부체크 되돌아감 방지.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        M = scanner.nextInt();
        scanner.nextLine();
        map = new int[N][M];
        visited = new int[N][M]; // 방문하면 1 , 안했으면 0

        for (int i = 0; i < N; i++) {
            String str = scanner.nextLine();
            for (int j = 0; j < M; j++) {
                map[i][j] = str.charAt(j) - '0';
            }
        }
        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();
        qx.add(0);
        qy.add(0);
        visited[0][0]=1;
        bfs(map,visited,qx,qy);
        System.out.println(visited[N-1][M-1]);
    }
    public static void bfs(int[][] map, int[][] visited, Deque<Integer> qx, Deque<Integer>qy){
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();
            int v = visited[y][x];
            //상
            if (y - 1 > -1 && visited[y - 1][x] == 0 && map[y - 1][x] == 1) {
                qy.add(y-1);qx.add(x);visited[y-1][x]=v+1;
            }
            //하
            if (y + 1 < N && visited[y + 1][x] == 0 && map[y + 1][x] == 1) {
                qy.add(y+1);qx.add(x);visited[y+1][x]=v+1;
            }
            //좌
            if (x - 1 > -1 && visited[y][x - 1] == 0 && map[y][x - 1] == 1) {
                qy.add(y);qx.add(x-1);visited[y][x-1]=v+1;
            }
            //우
            if (x + 1 < M && visited[y][x + 1] == 0 && map[y][x + 1] == 1) {
                qy.add(y);qx.add(x+1);visited[y][x+1]=v+1;
            }
        }
    }
}
