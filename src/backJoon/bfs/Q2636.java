package backJoon.bfs;


import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

//치즈
public class Q2636 {

    static int[][] ary;
    static int[][] visited;
    static int col;
    static int row;
    static int count=0;
    static int res=0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        col = scanner.nextInt();
        row = scanner.nextInt();
        ary=new int[col][row];
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                ary[i][j]= scanner.nextInt();
            }
        }

        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();
        qx.add(0);
        qy.add(0);
        sol(qx,qy);


    }
    public static void sol(Deque<Integer> qx, Deque<Integer> qy){
        visited=new int[col][row];
        int sum=0;
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();

            //위
            if (y - 1 >= 0 && visited[y-1][x]==0&&ary[y-1][x]==0) {
                visited[y-1][x]=1;
                qx.add(x);qy.add(y-1);

            }else if(y - 1 >=0 && visited[y - 1][x] == 0&& ary[y-1][x]==1){ ary[y-1][x]=0;visited[y-1][x]=1;sum++;}

            //아래
            if (y + 1 < col && visited[y + 1][x] == 0&& ary[y+1][x]==0) {
                visited[y+1][x]=1;
                qx.add(x);qy.add(y+1);

            }else if(y + 1 < col && visited[y + 1][x] == 0&& ary[y+1][x]==1) {ary[y+1][x]=0;visited[y+1][x]=1;sum++;}
                //왼쪽
            if (x - 1 >= 0 && visited[y][x - 1] == 0&&ary[y][x-1]==0) {
                visited[y][x-1]=1;
               qx.add(x-1);qy.add(y);

            }else if(x - 1 >=0 && visited[y][x-1] == 0&& ary[y][x-1]==1) {ary[y][x-1]=0;visited[y][x-1]=1;sum++;}
            //오른쪽
            if (x + 1 < row && visited[y][x + 1] == 0&&ary[y][x+1]==0) {
                visited[y][x+1]=1;
               qx.add(x+1);qy.add(y);

            }else if(x + 1 <row && visited[y][x+1] == 0&& ary[y][x+1]==1) {ary[y][x+1]=0;visited[y][x+1]=1;sum++;}
        }

        if (sum == 0) {
            if (count == 0) {
                System.out.println(count);
                System.out.println(res);
                return;
            }
            System.out.println(count);
            System.out.println(res);
            return;
        }

        if (sum != 0) {
            qx.add(0);qy.add(0);
            count++;
            res=sum;
            sol(qx, qy);
        }

    }
}
