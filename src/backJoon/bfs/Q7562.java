package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

//나이트의 이동
public class Q7562 {
    static int l;
    static int[] dx ={-2,-1,1,2,2,1,-1,-2};
    static int[] dy ={-1,-2,-2,-1,1,2,2,1};

    static int ey;
    static int ex;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();

        for (int t = 0; t < T; t++) {
            l = scanner.nextInt();
            int[][] ary = new int[l][l];
            int sy =scanner.nextInt();
            int sx =scanner.nextInt();
            ey =scanner.nextInt();
            ex =scanner.nextInt();

            Deque<Integer> qx = new ArrayDeque<>();
            Deque<Integer> qy = new ArrayDeque<>();

            qx.add(sx);qy.add(sy);
            ary[sy][sx]=1;
            sol(qx, qy, ary);

        }

    }

    public static void sol(Deque<Integer> qx,Deque<Integer> qy,int[][] ary) {


        while (!qx.isEmpty()&& ary[ey][ex]==0) {
            int a = qx.pop();
            int b = qy.pop();
            for (int i = 0; i < 8; i++) {
                int x = a+dx[i];
                int y = b+dy[i];
                if (x < 0 || y < 0 || x >= l || y >= l || ary[y][x] > 0) {
                    continue;
                }
                qx.add(x);
                qy.add(y);
                ary[y][x]=ary[b][a]+1;
            }
        }
        System.out.println(ary[ey][ex]-1);
    }
}
