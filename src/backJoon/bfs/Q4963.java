package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;
//섬의 개수
public class Q4963 {
    static int N;//세로
    static int M;//가로
    static int cnt;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            M = scanner.nextInt();
            N = scanner.nextInt();
            if (N == 0 && M == 0) {
                return;
            }
            int[][] map = new int[N][M];
            int[][] visited = new int[N][M];

            Deque<Integer> qx = new ArrayDeque<>();
            Deque<Integer> qy = new ArrayDeque<>();

            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    map[i][j] = scanner.nextInt();
                    if (map[i][j] == 1) {
                    }
                }
            }

            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    if (map[i][j] == 1 && visited[i][j] == 0) {
                        qx.add(j);qy.add(i);
                        bfs(map, visited, qx, qy);
                        cnt+=1;
                    }
                }
            }

            System.out.println(cnt);
            cnt=0;
        }

        }

    public static void bfs(int[][] map, int[][] visited, Deque<Integer> qx, Deque<Integer> qy) {
        while (!qx.isEmpty()) {
            int x = qx.pop();
            int y = qy.pop();

                //상
                if (y - 1 > -1 && visited[y - 1][x] == 0 && map[y - 1][x] == 1) {
                    qy.add(y - 1);qx.add(x);visited[y - 1][x] = 1;
                }
                //하
                if (y + 1 < N && visited[y + 1][x] == 0 && map[y + 1][x] == 1) {
                    qy.add(y + 1);qx.add(x);visited[y+1][x]=1;
                }
                //좌
                if (x - 1 > -1 && visited[y][x - 1] == 0 && map[y][x - 1] == 1) {
                    qy.add(y);qx.add(x - 1);visited[y][x-1]=1;
                }
                //우
                if (x + 1 < M && visited[y][x + 1] == 0 && map[y][x + 1] == 1) {
                    qy.add(y);qx.add(x + 1);visited[y][x+1]=1;
                }
                //좌상
                if (y - 1 > -1 && x - 1 > -1 && visited[y - 1][x-1] == 0 && map[y - 1][x-1] == 1) {
                    qy.add(y - 1);qx.add(x-1);visited[y - 1][x-1] = 1;
                }
                //우상
                if (y - 1 > -1 && x + 1 < M && visited[y-1][x + 1] == 0 && map[y-1][x + 1] == 1) {
                    qy.add(y-1);qx.add(x + 1);visited[y-1][x+1]=1;
                }
                //좌하
                if (y + 1 < N && x - 1 > -1 && visited[y+1][x - 1] == 0 && map[y+1][x - 1] == 1) {
                    qy.add(y+1);qx.add(x - 1);visited[y+1][x-1]=1;
                }
                //우하
                if (x + 1 < M && y + 1 < N && visited[y + 1][x+1] == 0 && map[y + 1][x+1] == 1) {
                    qy.add(y + 1);qx.add(x+1);visited[y+1][x+1]=1;
                }

        }
    }

}
