package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Scanner;

// 미로탐색 최단경로 -> BFS -> Queue
public class Q2178 {
    static int N;
    static int M;
    static int[][] map; // 경로 그래프
    static int[][] visited; // 방문 여부체크 되돌아감 방지.

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N= scanner.nextInt();
        M= scanner.nextInt();
        scanner.nextLine();
        map = new int[N][M];
        visited = new int[N][M];


        for (int i = 0; i < N; i++) {
            String str = scanner.nextLine();
            for (int j = 0; j < M; j++) {
                map[i][j] = str.charAt(j) - '0';
            }
        }

        Deque<Integer> qx = new ArrayDeque<>();
        Deque<Integer> qy = new ArrayDeque<>();
        // 시작점 큐에 삽입
        qx.add(0);
        qy.add(0);
        visited[0][0]=1;
        bfs(visited, map,qx,qy);

        System.out.println(visited[N-1][M-1]);
    }

    public static void bfs(int[][] visited, int[][] map,Deque<Integer> qx,Deque<Integer> qy){

        //큐안에 값이 없을때 까지
        while (!qx.isEmpty()) {

            int x = qx.pop();
            int y = qy.pop();

            int v = visited[y][x];
            // 방문한적 없고 & 이동할 수 있는 칸( 1 ) 이여야 함 visited[y][x] == 0 && map[y][x] == 1
            // 배열의 크기보다 크거나 작으면 안됨.

            //상
            if (y - 1 > -1 && visited[y - 1][x] == 0 && map[y - 1][x] == 1) {
                qy.add(y - 1);qx.add(x);visited[y - 1][x]=v+1;
            }
            //하
            if(y+1<N && visited[y+1][x] == 0 && map[y+1][x] == 1){
                qy.add(y+1); qx.add(x); visited[y + 1][x]=v+1;
            }
            //좌
            if(x-1>-1 && visited[y][x-1] == 0 && map[y][x-1] == 1){
                qy.add(y); qx.add(x-1);visited[y][x-1]=v+1;
            }
            //우
            if(x+1<M && visited[y][x+1] == 0 && map[y][x+1] == 1){
                qy.add(y); qx.add(x+1);visited[y][x+1]=v+1;
            }
        }

    }

}
