package backJoon.bfs;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

// 2606 바이러스
public class day2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M= scanner.nextInt();

        int[][] map = new int[N][N];
        int[][] visited = new int[N][N];


        for (int i = 0; i < M; i++) {
            int fr = scanner.nextInt();
            int to = scanner.nextInt();
            map[fr-1][to-1]=1;
            map[to-1][fr-1]=1;
            }
        Deque<Integer> que = new ArrayDeque<>();
        que.add(1);
        int answer =0;
        System.out.println(bfs(map,visited,que,answer));;
    }


    public static int bfs(int[][] map, int[][] visited, Deque<Integer> que,int answer) {

        while(que.size()>0){
            Integer fr = que.pop();
            for (int i = 0; i < map.length; i++) {
                visited[i][fr-1]=1;
            }
            for (int to = 0; to < map.length; to++) {
                if (visited[fr - 1][to] == 0 && map[fr - 1][to] == 1) {
                    if (!que.contains(to + 1)) {
                        que.add(to+1);
                        answer++;
                        System.out.println("que = " + que);
                    }


                }
            }
        }
        return answer;
    }
}
