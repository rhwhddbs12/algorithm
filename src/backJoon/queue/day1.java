package backJoon.queue;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class day1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int K = scanner.nextInt();
        int x = 1 ;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            list.add(i + 1);
        }
        System.out.print("<");
        while (list.size() != 1) {
            if (x != K) {
                list.add(list.get(0));
                list.remove(0);
                x++;
                continue;
            }else{
                System.out.print(list.get(0)+", ");
                list.remove(0);
                x=1;
            }
        }
        System.out.print(list.get(0));
        System.out.print(">");
    }
}
