package backJoon.queue;

import java.util.*;

public class day2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        Queue<Integer> que = new LinkedList<>();
        for (int i = 0; i < N; i++) {
            que.offer(i + 1);
        }
        for(int i =1;i<=N;i++) {
            que.offer(i);
        }
        while(que.size()!=1) {
            que.poll();
            que.offer(que.poll());
        }
        System.out.println(que.poll());
    }
}

//  add, remove vs offer, poll

//  add, remove를 사용하는것보다 offer 와 poll을 사용하는것이 효율이 더 좋다.

//add메소드는 해당 index~끝까지의 배열을 arraycopy로 옮겨 자리를 확보하고, 그 자리에 요소를 삽입하는 식으로 동작합니다
//
//문제에서 input이 최대 50만이라 했으므로,
//
//1개를 제거 - 49만~개를 복사 - 1개를 삽입
//
//같은 작업이 반복해서 일어나게 되고, 시간초과가 발생합니다

