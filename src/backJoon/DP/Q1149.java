package backJoon.DP;

import java.util.Scanner;

//RGB거리

/**
 * 1번 집의 색은 2번 집의 색과 같지 않아야 한다.
 * N번 집의 색은 N-1번 집의 색과 같지 않아야 한다.
 * i(2 ≤ i ≤ N-1)번 집의 색은 i-1번, i+1번 집의 색과 같지 않아야 한다.
 */
public class Q1149 {
    static int N;
    static int[][] ary;
    static int[][] cos;
    static int res=1000001;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();// 집의 수
        ary = new int[N][3]; // 세로:집번호 가로:RGB 색칠비용;
        cos = new int[N][3];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < 3; j++) {
                ary[i][j]= scanner.nextInt();
                cos[i][j]=ary[i][j];
            }
        }

        sol(1);
        res = Math.min(Math.min(cos[N-1][0],cos[N-1][1]),cos[N-1][2]);
        System.out.println(res);
    }
    public static void sol(int h){
        //종료조건
        if (h == N) {

            return;
        }
        for (int c = 0; c < 3; c++) {
            //RED
            if (c == 0) {
                cos[h][c]=Math.min(cos[h-1][c+1]+ary[h][c],cos[h-1][c+2]+ary[h][c]);
            }
            //GREEN
            if (c == 1) {
                cos[h][c]=Math.min(cos[h-1][c-1]+ary[h][c],cos[h-1][c+1]+ary[h][c]);
            }//BLUE
            if (c == 2) {
                cos[h][c]=Math.min(cos[h-1][c-1]+ary[h][c],cos[h-1][c-2]+ary[h][c]);
            }
        }

        sol(h+1);

    }
}
