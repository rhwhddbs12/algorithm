package backJoon.DP;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * 첫 줄에 물품의 수 N(1 ≤ N ≤ 100)과 준서가 버틸 수 있는 무게 K(1 ≤ K ≤ 100,000)가 주어진다.
 * 두 번째 줄부터 N개의 줄에 거쳐 각 물건의 무게 W(1 ≤ W ≤ 100,000)와 해당 물건의 가치 V(0 ≤ V ≤ 1,000)가 주어진다.
 */
//평범한 배낭     냅색 문제는 1. 쪼갤 수 있냐( 그리디) 2. 쪼갤 수 없음(DP)
public class Q12865 {
    static int N;
    static int K;
    static int[][] ary;
    static int[][] cost;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N= scanner.nextInt(); //물품의 수 N(1 ≤ N ≤ 100)
        K= scanner.nextInt(); //준서가 버틸 수 있는 무게 (1 ≤ K ≤ 100,000)
        ary = new int[N+1][2];
        for (int i = 1; i < N+1; i++) {
            ary[i][0]= scanner.nextInt();  // 물건의 무게 W(1 ≤ W ≤ 100,000)
            ary[i][1]= scanner.nextInt(); // 물건의 가치; V(0 ≤ V ≤ 1,000)
        }
        cost = new int[N+1][K+1];
        sol();
        System.out.println(cost[N][K]);
    }
    public static void sol(){
        //i = 물건 번호 j=는 1~K까지 무
        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < K+1; j++) {

                cost[i][j] = cost[i - 1][j];
                if (j - ary[i][0] > -1) {
                    cost[i][j]=Math.max(cost[i-1][j],cost[i-1][j-ary[i][0]]+ary[i][1]);
                }
            }
        }
    }
}
