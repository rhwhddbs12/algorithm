package backJoon.DP;

import java.util.Arrays;
import java.util.Scanner;

/**
  5
  7
  3 8
  8 1 0
  2 7 4 4
  4 5 2 6 5
 */
//정수 삼각형
public class Q1932 {
    static int N;
    static int[][] ary;
    static int[][] cost;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        ary = new int[N][N];
        cost = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j <= i; j++) {
                ary[i][j]= scanner.nextInt();
            }
        }
        cost[0][0]= ary[0][0];
        sol(1,2);
        int res= 0;
        for (int i = 0; i < N; i++) {
            if(res<cost[N-1][i])res=cost[N-1][i];
        }
        System.out.println(res);
    }
    // 대각선은 d=1 i라면 왼쪽대각선:i , 오른쪽 대각선:i+1;
    public static void sol(int d,int w){

        if (d == N) {
            return;
        }
        for (int i = 0; i < w; i++) {
            if(i==0) cost[d][i]=ary[d][i]+cost[d-1][i];
            else{
                cost[d][i] = Math.max(ary[d][i]+cost[d-1][i],ary[d][i]+cost[d-1][i-1]);
            }

        }

        sol(d+1,d+2);
    }
}
