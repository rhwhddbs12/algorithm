package backJoon.DP;

import java.util.Arrays;
import java.util.Scanner;

//1로 만들기
public class Q1463 {
    static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        sol();
    }
    public static void sol() {
     //********  bottom Up 방식!!  **************
        int[] d = new int[N+1];  // 메모제이션!
        for (int i = 2; i <= N; i++) {
            //-1
            d[i]=d[i-1]+1;  // +1은 안해주면 계속 0의 값이 나옴 즉 +1은 연산이 사용됬음을 알려줌
            //%2
            if(i%2==0) d[i]=Math.min(d[i],d[i/2]+1);
            //%3
            if(i%3==0) d[i]=Math.min(d[i],d[i/3]+1); // ex) 9 d[i/3]= d[3] =1 9를 3으로 나누면 3이남고 거기에 3을 한번 더 나눌꺼니까 +1
        }
        System.out.println(d[N]);
    }
}
