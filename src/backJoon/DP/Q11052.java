package backJoon.DP;
import java.util.Scanner;

//카드 구매하기
public class Q11052 {
    static int N;
    static int[] P;
    static int[] D;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        P = new int[N + 1];
        D = new int[N + 1];
        for (int i = 1; i <= N; i++) {
            P[i] = scanner.nextInt();
        }
        D[1]=P[1];
        sol();
    }

    public static void sol() {
        //bottom Up -> 반복문 사용

        for (int i = 2; i <= N; i++) {
            D[i]=P[i];

            for (int j = 1; j <= i; j++) {
                if (j > i - j) {
                    break;
                }
                if (D[i]<D[j] + D[i - j]) {
                    D[i]=D[j] + D[i - j];
                }
            }
        }
        System.out.println(D[N]);
    }
}
