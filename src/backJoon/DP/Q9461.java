package backJoon.DP;

import java.util.Arrays;
import java.util.Scanner;

//파도반 수열
public class Q9461 {
    static long[] P;
    static long[] res;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();

        res = new long[T];
        for (int i = 0; i < T; i++) {
            int N = scanner.nextInt();
            P=new long[N]; //초기화
            res[i] = sol(N);
        }

        for (int i = 0; i < T; i++) {
            System.out.println(res[i]);
        }
    }

    public static long sol(int N){
        for (int i = 0; i < N; i++) {
            if(i<=2) P[i] = 1;
            else if (i==3||i==4) P[i]=2;
            else P[i]= P[i-1]+P[i-5];
        }
        return P[N-1];
    }


}
