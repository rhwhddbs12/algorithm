package backJoon.DP;

import java.util.Arrays;
import java.util.Scanner;

/**  포도주 시식
 * 1.포도주 잔을 선택하면 그 잔에 들어있는 포도주는 모두 마셔야 하고, 마신 후에는 원래 위치에 다시 놓아야 한다.
 * 2.연속으로 놓여 있는 3잔을 모두 마실 수는 없다.
6
6
10
13
9
8
1
 * ex( 6, 10, 13, 9, 8, 1 )   ->세번째 값인 13 빼고 다 마시면 33으로 최대
 */
public class Q2156 {
    static int N;
    static int[] v;// 1~1000
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt(); //1~10000
        v = new int[N];
        for (int i = 0; i <N; i++) {
            v[i] = scanner.nextInt();
        }
        sol();
    }

    public static void sol(){
        //bottom up
        int[] d = new int[N];
        if(N>=1)d[0]=v[0];
        if(N>=2)d[1]=v[0]+v[1];
        if (N >= 3) {
            d[2]=Math.max(d[1],Math.max(v[0]+v[2],v[1]+v[2]));
            for (int i = 3; i < N; i++) {
                d[i]=Math.max(d[i-1],Math.max(d[i-3]+v[i-1]+v[i],d[i-2]+v[i])); //점화식
            }
        }
        System.out.println(d[N-1]);
    }
}
