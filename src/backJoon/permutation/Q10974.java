package backJoon.permutation;


import java.util.Scanner;

/**
 * 순열
 * 1. swap 이용
 * 2. DFS 이용
 */
public class Q10974 {
    static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        int[] res = new int[N];
        int[] visited = new int[N];
        int cnt=0;
        dfs(res,visited,0);
    }

    public static void dfs(int[]ary, int[] visited,int cnt) {
        //종료조건
        if (cnt == N) {
            for (int i = 0; i < N; i++) {
                System.out.print(ary[i]+" ");
            }
            System.out.println();
            return;
        }
        //재귀하면서 가장 중요한 부분은 전역변수가 아니라 지역변수로 호출이 이뤄진다는것.
        for (int i = 0; i < N; i++) {

            if (visited[i] == 0) {
                visited[i]=1; //방문한것은 1처리
                ary[cnt]= i+1; //index 0부터 시작했기때문에 +1 같을 정답배열로 넣어줌
                dfs(ary,visited,cnt+1); //DFS 재귀 부분

                //선택하지 않은경우
                ary[cnt]=0; //
                visited[i]=0;//
            }
        }

    }
}
