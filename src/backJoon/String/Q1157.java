package backJoon.String;

import java.util.Scanner;

public class Q1157 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        str = str.toUpperCase();

        char[] ary = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            ary[i]=str.charAt(i);
        }

        System.out.println(sol(ary));

    }

    public static char sol(char[] ary){
        int cnt =0;
        int tempCnt=0;
        char res='!';
        char tempRes= '!';
        for (int i = 0; i < ary.length; i++) {
            if(ary[i]!=0){
                tempRes = ary[i];
                for (int j = 0; j < ary.length; j++) {
                    if(tempRes == ary[j]){
                        ary[j]=0;
                        tempCnt++;
                    }
                }
            }
            if (tempCnt > cnt) {
                cnt =tempCnt;
                res =tempRes;
            } else if (tempCnt == cnt) {
                res='?';
            }
            tempCnt =0;
            tempRes ='!';
        }
        System.out.println(cnt);
        return res;
    }
}
