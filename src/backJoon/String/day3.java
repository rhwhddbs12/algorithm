package backJoon.String;

import java.util.Scanner;

//팰린드롬 수
public class day3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while(true){
            int num = scanner.nextInt();
            if (num == 0) {
                break;
            }
            String n = Integer.toString(num);
            int check=0;
            for (int i = 0; i < n.length()/2; i++) {
                int r = n.length()-1;
                if (n.charAt(i) != n.charAt(r-i)) {
                    check = 1;
                    System.out.println("NO");
                    break;
                }
            }
            if (check == 0) {
                System.out.println("YES");
            }

        }

    }
}
