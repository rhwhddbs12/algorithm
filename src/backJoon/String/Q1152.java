package backJoon.String;

import java.util.Scanner;
//A~Z = 65~90
//space = 32

/**
 * 단어의 개수
 */
public class Q1152 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int count =0;
        for (int i = 0; i < str.length(); i++) {


            if (str.charAt(i) == ' ') {
                if(i!=0&&i!=str.length()-1)
                count++;
            }
        }
        if (str.isEmpty()||(str.charAt(0)==' '&&str.length()==1)) {
            System.out.println("0");
        }else{
            System.out.println(count+1);
        }


    }
}
