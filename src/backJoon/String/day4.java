package backJoon.String;

import java.util.Scanner;

public class day4 {
    public static void main(String[] args) {
        int check=0;
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int qsize = scanner.nextInt();
        int[][] q= new int[qsize][2];

        StringBuilder str = new StringBuilder();

        //범위는 size크기와 같거나 작게 입력해야한다
        for (int i = 0; i < qsize; i++) {
            q[i][0]= scanner.nextInt();
            q[i][1]= scanner.nextInt();
        }

        for (int i = 0; i < qsize; i++) {
            for (int j = q[i][0]; j <= q[i][1]; j++) {
                str.append(arr[j-1]);
            }
            if (str.length() == 1) {
                check=1;
            }
            for (int j = 0;j<str.length()/2; j++) {
                    if (str.charAt(j) == str.charAt(str.length()-j-1)) {
                        check=1;
                    }else{
                        System.out.println("0");
                        break;
                    }
            }
            if (check == 1) {
                System.out.println("1");
                check=0;
            }
            str.setLength(0);
        }
    }
}
