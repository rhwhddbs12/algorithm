package backJoon.String;

import java.util.Scanner;

public class Q11720 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        String str = scanner.next();
        int temp =0;
        for (int i = 0; i < N; i++) {
            temp += Integer.parseInt(str.substring(i,i+1));
        }
        System.out.println("temp = " + temp);
    }
}
