package backJoon.greedy;

import java.util.Scanner;

public class day1 {
    static int answer = 0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int K = scanner.nextInt();
        int[] arr = new int[N];
        for (int i = 0; i < N; i++) {
            arr[i]= scanner.nextInt();
        }
        sol(N, K,arr);
    }

    static void sol(int N, int K, int[] arr) {
        for (int i = N - 1; i >= 0; i--) {
            if (K == 0) {
                System.out.println(answer);
                return;
            }
            if (K - arr[i]>=0) {
                answer++;
                K -= arr[i];
                i++; // 만약 3만원이면 1만원 빼고 또 1만원 빼고 1만원빼야하니까
            }
        }
    }
}
