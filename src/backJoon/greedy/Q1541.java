package backJoon.greedy;

import java.util.ArrayList;
import java.util.Scanner;
//잃어버린 괄호
public class Q1541 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        ArrayList<Integer> num = new ArrayList<>();
        ArrayList<Character> oper = new ArrayList<>();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < str.length(); i++) {
            //숫자 일때
            if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
                sb.append(str.charAt(i));
                if(i!=str.length()-1) continue;
            }
            num.add(Integer.parseInt(sb.toString()));
            sb=new StringBuffer();
            //연산기호 일때
            if (str.charAt(i) == 45 || str.charAt(i) == 43) {
                oper.add(str.charAt(i));
            }

        }
        int res=num.get(0);


        for (int i = 0; i < oper.size(); i++) {
            if (oper.get(i) == '-') {
                for (int j = i + 1; j < num.size(); j++) {
                    res-=num.get(j);
                }break;
            }else{
                res+=num.get(i+1);
            }
        }
        System.out.println(res);

    }
}
