package backJoon.greedy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class day2 {
    static boolean check = true;
    static int num = 0;
    static int answer =0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        List<String> list = new ArrayList<>();
        int count =0;
        int start = 0;

        for (int i = start; i < str.length(); i++) {
            if (str.charAt(i) == '-' || str.charAt(i) == '+') {
                list.add(str.substring(start, i));
                list.add(str.substring(i, i + 1));
                start = i+1;
                count++;
            }
            if (i == str.length()-1) {
                list.add(str.substring(start, str.length()));
            }
        }
        System.out.println(sol2(sol(list)));
    }
    static int sol2(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            // 괄호를 제외한 숫자 문자만 숫자로 형변환
            if (!list.get(i).equals("(") && !list.get(i).equals(")")) {
                num = Integer.parseInt(list.get(i));
            }else{
                if (list.get(i).equals("(")) check = false;
                else if (list.get(i).equals(")")) check =true;
                continue;
            }
            if(check) answer += num;
            else answer -= num;
        }
        return answer;
    }

    static List<String> sol(List<String> list){
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("-")) {
                list.add(i + 1, "(");
                for (int j = i + 1; j < list.size(); j++) {
                    if (list.get(j).equals("-")) {
                        list.add(j, ")");
                        break;
                    }
                    if (j == list.size() - 1) {
                        list.add(list.size(), ")");
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("+") || list.get(i).equals("-")) {
                list.remove(i);
            }
        }
        return list;
    }
}
