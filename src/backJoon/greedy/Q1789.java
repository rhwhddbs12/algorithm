package backJoon.greedy;

import java.util.Scanner;

public class Q1789 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Long S = scanner.nextLong();
        Long T = S;
        int N = 0;
        int i = 1;
        while(T-i>=0){
            T-=i;
            i++;
            N++;
        }
        System.out.println(N);
    }
}
