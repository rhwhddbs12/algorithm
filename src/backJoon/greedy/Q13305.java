package backJoon.greedy;

import java.util.Scanner;

//주유소

/**
 * 입력 값
 * 4
 * 2 3 1
 * 5 2 4 1
 */
public class Q13305 {

    static int N;
    static long[] price;
    static long[] dis;
    static long res=0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();//도시의 개수  N(2 ≤ N ≤ 100,000)
        price = new long[N]; //기름 가격
        dis = new long[N - 1]; // 도시사이의 거리

        for (int i = 0; i < N-1; i++) {
            dis[i]= scanner.nextLong();
        }
        for (int i = 0; i < N; i++) {
            price[i]= scanner.nextLong();
        }
        sol();
    }

    public static void sol() {

        for (int i = 0; i < price.length; i++) {

            for (int j = i; j < i+check(i)+1; j++) {// 현재 도시의 비용부터 비용이 더 비싼 도시까지
                if (j < price.length - 1) {
                    res+= price[i]*dis[j];
                }
            }
            i = i + check(i);
        }
        System.out.println(res);
    }
    //현재 기름값이 이어지는 다른 count개의 도시보다 싸다
    public static int check(int x){
        int count=0;

        for (int i = x; i < N; i++) {
            if (price[x] < price[i]) {
                count++;
            }else{
                break;
            }
        }
        return count;
    }

}
