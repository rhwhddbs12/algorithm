package backJoon.backTracking;

import java.util.Scanner;

//N-Queen BackTracking
public class Q9663 {
    static int N;
    static int[] ary; // ex) {0,1,2,3} index=열 ary[index]= Queen의 위치
    static int cnt;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        ary = new int[N];
        sol(0);
        System.out.println(cnt);
    }

    public static void sol(int d){
        //종료조건
        if (d == N) {
            cnt++;
            return;
        } else {
            //d번째 열의 칸에 Queen을 위치 시키기
            for (int i = 0; i < N; i++) {
                ary[d]=i;
                //d번째 열의 칸에 Queen을 위치시키는것이 가능하면 다음열로(d+1)
                if (possible(d)) {
                    sol(d+1);
                }
            }

        }
    }
    //d번째 열의 칸에 Queen이 위치될 수 있는지 검사
    public static boolean possible(int d) {
        for (int i = 0; i < d; i++) {
            //같은열일 때
            if (ary[d] == ary[i]) {
                return false;
            }
            //대각선일 때    열:(d-i)  행:(ary[d]-ary[i])  아래 비교에서 같으면 (1,1)에서 같은 수만큼 행과열이 움직여서 대각선이됨 ex)(2,2),(3,3) 다르면 ex) (2,3)
            else if (Math.abs(d - i) == Math.abs(ary[d] - ary[i])) {
                return false;
            }
        }
        return true;
    }
}

