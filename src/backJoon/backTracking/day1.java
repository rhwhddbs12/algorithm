package backJoon.backTracking;

import java.util.Scanner;

public class day1 {
    static Scanner scanner = new Scanner(System.in);
    static int N = scanner.nextInt(); //1부터 N까지
    static int M = scanner.nextInt(); //중복없이 M개를 고른다.
    static boolean[] v = new boolean[N];
    static int output[] = new int[N];

    public static void main(String[] args) {
        back(0);
    }

    static void back(int depth) {
        if (depth == M) {
            for (int i = 0; i < M; i++) {
                System.out.print(output[i]+" ");
            }
            System.out.print("\n");
            return ;
        }
        for (int i = 0; i < N; i++) {
            if (!v[i]) {
                v[i]=true;
                output[depth] = i+1;
                back(depth+1);
                v[i] = false;
            }
        }
    }
}
