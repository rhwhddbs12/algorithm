package backJoon.math;

import java.util.Scanner;
//에라토스테네스의 체
public class Q1929 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M = scanner.nextInt();
        //배열의 인덱스는 실제 숫자를 나타냄 check[3] = 3
        boolean[] check = new boolean[M+1];  //M+1은 1~100이아니라 0~100이므로

        check[0]=check[1]=true;
        //N의  소수를 구하려면 2~루트N 까지 구하면 된다.
        for (int i = 2; i * i < M; i++) {
            if (check[i]) {
                continue;
            }
            //소수의 배수들은 전부 제거한다. ex) 2는 소수지만 2의배수들은 2를 포함하기때문에 소수가 될 수 없음.
            for (int j = i + i; j <= M; j += i) {
                check[j]=true;
            }

        }
        for (int i = N; i <= M; i++) {
            if(!check[i])
            System.out.print(i+" ");
        }
    }
}
