package backJoon.sorting;

import java.util.*;

public class day7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int answer = 0;
        int counter = 0;
        List<Integer> weight = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            weight.add(i, scanner.nextInt());
        }
        Collections.sort(weight, Collections.reverseOrder());
        System.out.println("weight = " + weight);

        for (int i = 0; i < weight.size() ; i++) {
            //제일작은값 * 로프 수
            counter++;
            if (answer < weight.get(i) * counter) {
                answer = weight.get(i) * counter;
            }
        }

        System.out.println(answer);
    }
}
