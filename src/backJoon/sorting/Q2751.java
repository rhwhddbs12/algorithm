package backJoon.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

//수 정렬하기 Quick Sort
public class Q2751 {
    static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        N = scanner.nextInt();
        ArrayList<Integer> ary = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            ary.add(scanner.nextInt());
        }

        Collections.sort(ary);
        for (int i = 0; i < N; i++) {
            sb.append(ary.get(i));
            sb.append("\n");
        }
        System.out.println(sb);
    }

}
