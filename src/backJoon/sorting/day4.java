package backJoon.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class day4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        String[][] str = new String[size][2];
        for (int i = 0; i < size; i++) {
            str[i][0] = scanner.next();
            str[i][1] = scanner.next();
        }

        Arrays.sort(str, new Comparator<String[]>() {
            @Override
            public int compare(String[] o1, String[] o2) {
//                return o1[0].compareTo(o2[0]) ;
                return Integer.parseInt(o1[0])-Integer.parseInt(o2[0]) ;
            }
        });
        for (int i = 0; i < size; i++) {
            System.out.println(str[i][0]+" "+str[i][1]);
        }


    }


}
