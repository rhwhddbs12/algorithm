package backJoon.sorting;

import java.util.Arrays;
import java.util.Scanner;

public class day1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(solution(arr));
    }
    public static int solution(int[] arr){
        int answer = 0;
        int i =0;
        Arrays.sort(arr);
        while (i != arr.length) {
            for (int j = 0; j <= i; j++) {
                answer += arr[j];
            }
            i++;

        }
        return answer;
    }

}
