package backJoon.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class day6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();

        int[][] arr = new int[size][2];
        for (int i = 0; i < arr.length; i++) {
            arr[i][0]= scanner.nextInt();
            arr[i][1]= scanner.nextInt();
        }
        Arrays.sort(arr, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[0] == o2[0]) {
                    return o1[1]-o2[1];
                }
                return o1[0]-o2[0];
            }
        });
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][0]+" "+arr[i][1]);
        }
    }
}
