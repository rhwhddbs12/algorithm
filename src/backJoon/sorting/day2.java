package backJoon.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class day2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[][] arr = new int[size][2];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j]= scanner.nextInt();
            }
        }

        System.out.println(solution(arr));
    }
    public static int solution(int[][] arr){
        int answer =0;
        int start =-1;

        //종료시간이 빠른순으로 정렬하되 같은경우 시작시간이 경우가 먼저 정렬
        Arrays.sort(arr, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[1] == o2[1]) {
                    return o1[0]-o2[0];
                }
                return o1[1]-o2[1];
            }
        });
        System.out.println(Arrays.deepToString(arr));
            for (int i = 0; i < arr.length; i++) {
                if (arr[i][0]>= start) {
                    if (arr[i][0] == arr[i][1]) {
                        start = arr[i][1];
                        answer++;

                    }else{
                        answer++;
                        start = arr[i][1];
                        System.out.println("end:"+arr[i][1]);
                    }

                }
            }
        return answer;
    }
}
