package backJoon.sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class day3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        String[] str = new String[size];
        for (int i = 0; i < size; i++) {
            str[i] = scanner.next();
        }
        Arrays.sort(str, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.length() == o2.length()) {
                    return o1.compareTo(o2);
                }
                return o1.length()-o2.length();
            }
        });
        for (int i = 0; i < size; i++) {
            if (i > 0 && i < size) {
                if (str[i].equals(str[i - 1])) {
                    continue;
                }else{
                    System.out.println(str[i]);
                }
            }else{
                System.out.println(str[i]);
            }

        }
        
    }
}
