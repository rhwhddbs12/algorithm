package backJoon.dfs;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Scanner;
//영역 구하기
public class Q2583 {
    static int M;
    static int N;
    static int K;
    static int cnt;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        M= scanner.nextInt();
        N= scanner.nextInt();
        K= scanner.nextInt();

        int fx;
        int fy;
        int tx;
        int ty;
        int[][] map = new int[M][N];
        int[][] visited = new int[M][N];
        int section=0;
        for (int i = 0; i < K; i++) {
            fx= scanner.nextInt();fy= scanner.nextInt();tx= scanner.nextInt();ty= scanner.nextInt();
            makeMap(map, visited, fx, fy, tx, ty);
        }

        ArrayList<Integer> ary = new ArrayList<>();
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (map[i][j] == 0 && visited[i][j] == 0) {
                    dfs(map, visited, i,j);
                    section+=1;
                    ary.add(cnt);
                    cnt=0;
                }
            }
        }
        System.out.println(section);
        for (int i = 0; i < ary.size(); i++) {
            Collections.sort(ary);
            System.out.print(ary.get(i)+" ");
        }
    }

    public static void dfs(int[][] map, int[][] visited, int f,int t) {
            visited[f][t]=1;
            cnt+=1;
        if (f >= 0 && t >= 0 && f < M && t < N) {
            //상
            if (f - 1 > -1 && visited[f - 1][t] == 0 && map[f - 1][t] == 0) {
                dfs(map,visited,f-1,t);
            }
            //하
            if (f + 1 < M && visited[f + 1][t] == 0 && map[f + 1][t] == 0) {
                dfs(map,visited,f+1,t);
            }
            //좌
            if (t - 1 > -1 && visited[f][t - 1] == 0 && map[f][t - 1] == 0) {
                dfs(map,visited,f,t-1);
            }
            //우
            if (t + 1 < N && visited[f][t + 1] == 0 && map[f][t + 1] == 0) {
                dfs(map, visited, f,t + 1);
            }
        }

    }

    public static void makeMap(int[][] map,int[][] visited,int fx, int fy, int tx, int ty) {
        for (int i = fy; i < ty; i++) {
            for (int j = fx; j < tx; j++) {
                map[i][j]=1;visited[i][j]=1;
            }
        }
    }


}
