package backJoon.dfs;

import java.util.Scanner;

//유기농 배추
public class Q1012 {
    static int T; //테스트 케이스 개수
    static int M; //밭 가로의 길이
    static int N; //밭 세로의 길이
    static int K; //배추 갯수
    static int cnt; //구역
    static int[][] visited;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        T = scanner.nextInt();

        for (int i = 0; i < T; i++) {
            M = scanner.nextInt();
            N = scanner.nextInt();
            K = scanner.nextInt();
            cnt=0;
            int[][] map = new int[N][M];
            visited = new int[N][M];
            for (int j = 0; j < K; j++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                map[y][x] = 1;
            }
            for (int y = 0; y < N; y++) {
                for (int x = 0; x < M; x++) {
                    if (map[y][x] == 1 && visited[y][x] == 0) {

                        dfs(map, visited,y, x);
                        cnt+=1;
                    }
                }
            }

            System.out.println(cnt);
        }
    }

    public static void dfs(int[][] map, int[][] visited, int y,int x){
        visited[y][x]=1;
        if (y >= 0 && x >= 0 && y < N && x < M) {

            //상
            if (y - 1 > -1 && map[y - 1][x] == 1 && visited[y - 1][x] == 0) {
                dfs(map, visited, y - 1, x);
            }
            //하
            if (y + 1 < N && map[y + 1][x] == 1 && visited[y + 1][x] == 0) {
                dfs(map, visited, y + 1, x);
            }
            //좌
            if (x - 1 > -1 && map[y][x - 1] == 1 && visited[y][x - 1] == 0) {
                dfs(map, visited, y, x - 1);
            }
            //우
            if (x + 1 < M && map[y][x + 1] == 1 && visited[y][x + 1] == 0) {
                dfs(map, visited, y, x + 1);
            }
        }
    }
}