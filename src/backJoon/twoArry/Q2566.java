package backJoon.twoArry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Q2566 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] ary = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                ary[i][j] = scanner.nextInt();
            }
        }
        sol(ary);
    }

    public static void sol(int[][]ary) {

        int res = ary[0][0];
        int row = 1;
        int col = 1;

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (ary[i][j] > res) {
                    res = ary[i][j];
                    row =i+1;
                    col =j+1;
                }
            }
        }
        System.out.println(res);
        System.out.print(row+" "+col);
    }
}
