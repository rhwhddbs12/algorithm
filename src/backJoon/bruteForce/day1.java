package backJoon.bruteForce;

import java.util.Arrays;
import java.util.Scanner;

// n장의 카드중 3장의 카드를 골라 M에 가장 가까운 수 를 만들어라.
// 순서가 중요하면 순열 (퍼뮤테이션)
// 순서가 필요없으면 조합 (컴비네이션)

public class day1 {
    static int answer =0; //최종 접당
    static int sum =0; // 조합가능한 수들의
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); // 전체 카드 수
        int M = scanner.nextInt(); // 총 합
        int[] arr = new int[N]; // 숫자 카드 배열
        int[] output = new int[N]; // 조합 가능한 카드 배열
        boolean[] visited = new boolean[N]; //방문여부

        // 카드배열 초기 값
        for (int i = 0; i < N; i++) {
            arr[i] = scanner.nextInt();
        }
        per(arr,output,visited,N,0,M);
        System.out.println(answer);
    }

    public static void per(int[] arr, int[] output, boolean[] visited, int n, int depth, int M) {
        // 카드 세장을 다 뽑았을 경우
        if (depth == 3) {
            //카드 세장의 총 합
            for (int i = 0; i < 3; i++) {
                sum += output[i];
            }
            //카드 세장의 총 합중 M과 가장 가까운 수
            if (answer < sum && sum <= M) {
                answer = sum;
            }
            sum=0; // 총합 초기화
            return ;
        }
        //카드 배열 크기 만큼 루프
        for (int i = 0; i < n; i++) {
            //방문하지 않았을 경우
            if (visited[i] == false) {
                visited[i] = true;
                output[depth] = arr[i]; // 이 부분에서 output[0]=1 output[0]=2 output[0]=3 각각 들어갈 수 있음
                per(arr, output, visited, n, depth + 1, M);
                visited[i] = false; // 이 부분 체크를 안해주면 visited[i]를 한 번 밖에 돌 수 없음.
            }
        }
    }


}
