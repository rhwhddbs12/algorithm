package backJoon.bruteForce;

import java.util.Scanner;

public class day2 {
    static StringBuilder sb = new StringBuilder();
    static int sum = 0;
    static int answer = 0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); //
        String M = Integer.toString(N);
        int f =Character.getNumericValue(M.charAt(0)); // char to int
        int[] arr = new int[M.length()];
        int[] output = new int[M.length()];
        boolean[] visited = new boolean[M.length()];
        sol(M,f,arr,output,visited,0);
        System.out.println(answer);
    }

    static void sol(String M,int f,int[] arr,int[] output,boolean[] v,int depth) {
        if (depth == M.length()) {
            for (int i = 0; i < arr.length; i++) {
                sb.append(Integer.toString(output[i]));
                sum += output[i];
            }

            sum += Integer.parseInt(sb.toString());

            if (sum == Integer.parseInt(M)) {
                if (answer == 0) {
                    answer = Integer.parseInt(sb.toString());
                }
                else if(answer> Integer.parseInt(sb.toString()))
                    answer = Integer.parseInt(sb.toString());
            }
            sum=0;
            sb.setLength(0);

            return;
        }
        for (int i = 0; i <= 9; i++) {
            if (output[0] <= f) {
              output[depth]=i;
              sol(M, f, arr, output, v, depth+1);
            }
        }
    }

}
