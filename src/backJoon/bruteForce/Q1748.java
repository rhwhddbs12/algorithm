package backJoon.bruteForce;

import java.util.Scanner;
//수 이어 쓰기 1
public class Q1748 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long ans = 0;

//        for (int i = 1; i <= N ; i++) {
//            ans += (int)(Math.log10(i)+1);
//        } // ->120 번 연산
        for (int start=1, len=1; start<=n; start*=10, len++) {
            int end = start*10-1;  // 9, 99,999.....
            if (end > n) {
                end = n;
            }
            ans += (long)(end - start + 1) * len; //  1의자리:(9-1+1)*1=9 , 10의자리:(99-10+1)*=180, 120:(120-100+1)*3= 63
        }  //-> 3번 연산
        System.out.println(ans);
    }
}
