package backJoon.bruteForce;

import java.util.Scanner;
//카잉 달력
public class Q6064 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T= scanner.nextInt();
         int[] M = new int[T];
         int[] N = new int[T];
         int[] x = new int[T];
         int[] y = new int[T];
        for (int i = 0; i < T; i++) {
            M[i] = scanner.nextInt() ;
            N[i] = scanner.nextInt() ;
            x[i] = scanner.nextInt() -1;
            y[i] = scanner.nextInt() -1;
        }
        for (int i = 0; i < T; i++) {
            System.out.println(sol(M[i], N[i], x[i], y[i]));
        }
    }
    public static int sol(int M, int N, int x, int y) {
        int ans = -1;
        for (int i = 0; i <= M*N; i++) {
            if ((i * M + x ) % N == y) {
                ans = (i * (M) + x+1 );
                break;
            }
        }
        return ans;
    }
}
