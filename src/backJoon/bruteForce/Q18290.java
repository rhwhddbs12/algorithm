package backJoon.bruteForce;

import java.util.Scanner;
//1 ≤ N, M ≤ 10
//1 ≤ K ≤ min(4, N×M)
public class Q18290 {
    static int[] dx = {0, 1, 0, -1};
    static int[] dy = {1, 0, -1, 0};
    static int cnt = 0;
    static int sum = 0;
    static int ans = 0;

    static Scanner scanner = new Scanner(System.in);
    static int N = scanner.nextInt();
    static int M = scanner.nextInt();
    static int K = scanner.nextInt();
    static int[][] arr = new int[N][M];
    static boolean[][] check = new boolean[N][M];

    public static void main(String[] args){

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }
        sol(cnt,sum);
        System.out.println(ans);
    }


    static void sol(int cnt, int sum){
        // 종료조건
        if (cnt == K) {
            if(ans<sum)ans=sum; // 가장 큰 합으로 정답 체크
            return;
        }
        // 칸 선택 조건
        for (int x = 0; x < N; x++) {
            for (int y = 0; y < M; y++) {
                if(check[x][y]) continue;
                boolean ok = true;
                //인접하여 칸을 선택 못함
                for (int i = 0; i < 4; i++) {
                    int nx = x + dx[i];
                    int ny = y + dy[i];
                    if (0 <= nx && nx < N && 0 <= ny && ny < M) { //인접할 조건
                        if(check[nx][ny]) ok = false;
                    }
                }
                //칸을 선택 함
                if (ok) {
                    check[x][y] = true;
                    sol(cnt+1,sum+arr[x][y]);
                    check[x][y] = false;
                }
            }
        }
    }
}
