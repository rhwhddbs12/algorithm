package backJoon.bruteForce;

import java.util.ArrayList;
import java.util.Scanner;

public class Q14889 {
    static int N; // N(4 ≤ N ≤ 20, N은 짝수)
    static int[][] ary;
    static int res = 999;
    static int min = 999; // f-s
    static int index = 0;
    static boolean[] map;
    static ArrayList<Integer> f = new ArrayList<>();
    static ArrayList<Integer> s = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N= scanner.nextInt();
        map = new boolean[N];
        ary = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                ary[i][j] = scanner.nextInt();
            }
        }

        sol(0, 0);

        System.out.println(res);
    }

    public static void sol(int index, int cnt) {
        // 종료조건 이미 최소를 만족한 경우
        if (index == N) {
            if (cnt == N / 2) {
                for (int i = 0; i < N; i++) {
                    if (map[i]) {
                        f.add(i);
                    }else{
                        s.add(i);
                    }
                }
                cal(f, s); // 능력치 절대값 계산
                if (min == 0) {
                    res = min;
                    return;
                } else {
                    if (res > min) {
                        res = min;
                    }
                }
            }
            f.removeAll(f);
            s.removeAll(s);
            return;
        }
        map[index]=true;
        sol(index+1,cnt+1);
        map[index] = false;
          sol(index+1,cnt);
    }

    public static int cal(ArrayList<Integer> f, ArrayList<Integer> s) {
        int fsum = 0;
        int ssum = 0;

        for (int i = 0; i <N; i++) {
            for (int j = 0; j <N; j++) {
                if (map[i] && map[j]) {
                    fsum += ary[i][j];
                }
                if (!map[i] && !map[j]) {
                    ssum += ary[i][j];
                }

            }
        }

        min = Math.abs(fsum - ssum);
        return min;
    }
}
