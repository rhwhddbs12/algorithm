package backJoon.bruteForce;

import java.util.Arrays;
import java.util.Scanner;

/**
 1. 암호는 서로다른 L개의 알파벳
 2. 최소 한개의 모음(aeiou)와 최소 두개의 자음으로 구성
 3. 오름차순 정렬 ex) abc(o) bac(x)
 4. 알파벳은 소문자 only
 5. 중복 x
 */
public class Q1759 {
    static int MO = 0;
    static int JA = 0;
    static int index = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int L = scanner.nextInt(); //서로 다른 L개
        int C = scanner.nextInt(); //문자의 종류
        String[] arr = new String[C];
        String res = "";
        for (int i = 0; i < C; i++) {
            arr[i] = scanner.next();
        }
        Arrays.sort(arr); /** 3. 오름차순*/
        sol(arr, index, res, L);

    }
    /** 재귀호출 문 */        //a, t, c, i, s, w
    public static void sol(String[] arr, int index, String res,int L){
        if (res.length() == L) {
            //모든 조건을 만족했을 경우 출력
            if(checking(res)){
                System.out.println(res);
            }
            return;
        }
        //선택했을때
        if (index >= arr.length) { return; }
        sol(arr, index+1, res+arr[index], L);
        //선택안했을때
        sol(arr, index+1, res, L);
    }

    /** 모음과 자음 갯수 체킹 */
    public static boolean checking(String res){
        int mo = 0;
        int ja = 0;

        for (char x : res.toCharArray()) {
            if (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u') {
                mo++;
            } else {
                ja++;
            }
        }

//        char[] b = res.toCharArray();
//        for (int i = 0; i < res.length(); i++) {
//            if (b[i] == 'a' || b[i] == 'e' || b[i] == 'i' || b[i] == 'o' || b[i] == 'u') {
//                mo++;
//            } else {
//                ja++;
//            }
//        }

        if (mo >= 1 && ja >= 2) {
            return true;
        }else{
            return false;
        }
    }
}
