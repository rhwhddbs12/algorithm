package backJoon.bruteForce;
//1-1 9명의 난장이중 7명의 난장이를 찾는다. 9C7
//1-2 9명의 난장이중 2명의 난장이를 찾는다. 9C2    O(N제곱)
//2 키의 핪을 구한다.  O(N)

import java.util.*;

public class Q2309 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        int sum=0;
        for (int i = 0; i < 9; i++) {
            list.add(scanner.nextInt());
            sum += list.get(i);
        }
        loop:
        for (int i = 0; i < list.size()-1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if (sum - list.get(i) - list.get(j) == 100) {
                    list.remove(list.get(j));
                    list.remove(list.get(i));
                    break loop;
                }
            }
        }
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i)+" ");
        }

    }
}
