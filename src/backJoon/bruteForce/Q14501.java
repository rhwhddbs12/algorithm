package backJoon.bruteForce;

import java.util.Scanner;

public class Q14501 {
    static int Tsum=0;
    static int index = 0;
    static int res = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();

        int[] T = new int[N];
        int[] P = new int[N];
        for (int i = 0; i < N; i++) {
            T[i]= scanner.nextInt();
            P[i]= scanner.nextInt();
        }
        sol(T,P,index,Tsum);
        System.out.println(res);
    }

    public static void sol(int[] T, int[] P, int index,int Psum){
        if (index >= T.length) {
            if (Psum > res) {
                res = Psum;
            }
            return;
        }
        if (index + T[index] <= T.length) {
            sol(T, P, index + T[index], Psum + P[index]);
        }
        if (index + 1 <= T.length) {
            sol(T, P, index + 1, Psum);
        }

    }
}
