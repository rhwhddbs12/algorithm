package backJoon.bruteForce;

import java.util.Scanner;

//N과M (1.순서:N! , 2.선택:2의N제곱)

public class Q15649 {
    static boolean[] arr = new boolean[10]; //인덱스 번호
    static int[] ans = new int[10]; // 선택된 번호
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M = scanner.nextInt();
        int index = 0;
        sol(index, N, M);
    }
    static void sol(int index, int N, int M) {
        if (index == M) {
            for (int i = 0; i < M; i++) {
                System.out.print(ans[i]+" ");
            }
            System.out.println("");
            return;
        }
        for (int i = 1; i <= N; i++) {

            if(arr[i]) continue;
            arr[i]=true; ans[index]=i;
            sol(index + 1, N, M);
            arr[i]=false;
        }

    }
}
