package backJoon.bruteForce;

import java.util.Scanner;
//N과 M(2) (오름차순)
public class Q15650 {
    static int[] ans = new int[10]; // 선택된 번호
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M = scanner.nextInt();
        int index = 0;
        int start = 1;
        sol(index,start, N, M);
    }
    static void sol(int index,int start, int N, int M) {
        if (index == M) {
            for (int i = 0; i < M; i++) {
                System.out.print(ans[i]+" ");
            }
            System.out.println("");
            return;
        }
        for (int i = start; i <= N; i++) {
            ans[index]=i;
            sol(index + 1,i+1, N, M);

        }
    }
}
