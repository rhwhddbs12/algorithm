package backJoon.bruteForce;

import java.util.Scanner;

//영화감독 숌
public class Q1436 {
    static int N;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();

        sol();

    }
    public static void sol() {
        int count=1;
        int num=666;
        while (count != N) {
            num++;
            if (String.valueOf(num).contains("666")) {
                count++;
            }
        }
        System.out.println(num);
    }
}
