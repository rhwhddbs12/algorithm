package backJoon.bruteForce;

import java.util.Scanner;

//날짜계산  1 ≤ E ≤ 15, 1 ≤ S ≤ 28, 1 ≤ M ≤ 19)
public class Q1476 {
    static Scanner scanner = new Scanner(System.in);
    static int E = scanner.nextInt();
    static int S = scanner.nextInt();
    static int M = scanner.nextInt();
    static int X = 0;

    // X%15== E && X%28 ==S && X%19 == M
    public static void main(String[] args) {
        boolean a = true;
        while (a) {
            if (X % 15 == E-1 && X % 28 == S-1 && X % 19 == M-1) {
                System.out.println(X+1);
                a = false;
                break;
            }else{
                X++;
            }
        }

    }
}
