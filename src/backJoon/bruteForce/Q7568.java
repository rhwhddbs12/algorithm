package backJoon.bruteForce;

import java.util.Arrays;
import java.util.Scanner;

//덩치
public class Q7568 {
    static int N;
    static int[] k;
    static int[] h;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        k = new int[N];
        h = new int[N];
        for (int i = 0; i < N; i++) {
            k[i]= scanner.nextInt();
            h[i]= scanner.nextInt();
        }
        sol();
    }

    public static void sol(){
        StringBuilder sb= new StringBuilder();

        for (int i = 0; i < N; i++) {
            int r=1;
            for (int j = 0; j < N; j++) {
                if (h[i] < h[j] && k[i] < k[j]) {
                    r+=1;
                }
            }
            sb.append(r+" ");
        }
        System.out.println(sb);
    }

}
