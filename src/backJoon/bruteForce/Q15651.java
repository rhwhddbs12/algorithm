package backJoon.bruteForce;

import java.io.*;
import java.util.StringTokenizer;

public class Q15651 {
    static int[] ans = new int[10];
    static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in)); //선언
        StringTokenizer st = new StringTokenizer(bf.readLine()); //Read한 데이터를 가공
        int N = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());
        bf.close();
        int index = 0;
        sol(index, N,M);
        bw.flush(); //BufferedWriter는 마지막에 flush해서 비워줘야함 이거 안하면 안됨!
    }
    static void sol(int index, int N, int M) throws IOException {
        if (index == M) {
            for (int i = 0; i < M; i++) {
                bw.write(ans[i]+" ");// sout처럼 쓰임
            }
            bw.newLine();//개행
            return;
        }
        for (int i = 1; i <= N; i++) {
            ans[index]=i;
            sol(index + 1, N, M);
        }
    }
}
