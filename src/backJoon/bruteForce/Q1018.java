package backJoon.bruteForce;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 체스판 다시 칠하기
 */
public class Q1018 {
    static int N;
    static int M;
    static boolean[][] ary;

    static int min = 64;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        N = scanner.nextInt();
        M = scanner.nextInt();

        scanner.nextLine();

        ary = new boolean[N][M];
        for (int i = 0; i < N; i++) {
            String str = scanner.nextLine();
            for (int j = 0; j < M; j++) {
                if(str.charAt(j)=='W') ary[i][j] = true;
                else ary[i][j]=false;
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (i + 7 < N && j + 7 < M) {
                    sol(i, j);
                }
            }
        }
        System.out.println(min);
    }

    public static void sol(int y, int x) {
        boolean start = ary[y][x];
        int count=0;
        for (int i = y; i <y+ 8; i++) {
            for (int j = x; j <x+ 8; j++) {
                if (ary[i][j] != start) {
                    count++;
                }
                start=(!start);
            }
            start=(!start);
        }

        //첫번째 칸의 색을 바꿀 때 64-count
        count = Math.min(count, 64 - count);
        min = Math.min(min, count);
    }

}

