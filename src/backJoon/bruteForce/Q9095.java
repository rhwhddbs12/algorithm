package backJoon.bruteForce;

import java.util.Scanner;
//1,2,3 더하기

public class Q9095 {
    static int ans = 0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int T = scanner.nextInt();
        for (int i = 0; i < T; i++) {
            int N = scanner.nextInt();
            sol(0, 0, N);
            System.out.println(ans);
        }

    }

    static int sol(int count, int sum, int N) {
        if(sum>N) return 0; // 안되는 경우
        if(sum==N) return  1; // 되는 경
        ans = 0;
        for (int i = 1; i <=3; i++) {
            ans +=  sol(count + 1, sum + i, N);
        }
        return ans;
    }
}
