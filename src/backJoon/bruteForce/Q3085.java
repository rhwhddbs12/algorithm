package backJoon.bruteForce;
import java.util.Scanner;
//사탕게임
public class Q3085 {

    static Scanner scanner = new Scanner(System.in);
    static int cnt = 1; // 인접한 사탕의 최대 개수
    static int ans = 1; // 정답
    static int N = scanner.nextInt();
    static String[][] arr = new String[N][N];

    public static void main(String[] args) {

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                arr[i][j] = scanner.next();
            }
        }
        System.out.println(sol());
    }
    public static int sol(){
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                //오른쪽
                if (j + 1 < N) {
                    String t = "";
                    //인접한 두 색 swap
                    t= arr[i][j]; arr[i][j]=arr[i][j+1]; arr[i][j+1]=t;
                    cnt = check(arr);
                    if (ans < cnt) ans=cnt;
                    //초기화
                    t= arr[i][j]; arr[i][j]=arr[i][j+1]; arr[i][j+1]=t;
                }
                //END 오른쪽
                //아래
                if (i + 1 < N) {
                    String t = "";
                    //인접한 두 색 swap
                    t= arr[i][j]; arr[i][j]=arr[i+1][j]; arr[i+1][j]=t;
                    cnt = check(arr);
                    if (ans < cnt) ans=cnt;
                    //초기화
                    cnt=1;
                    t= arr[i][j]; arr[i][j]=arr[i+1][j]; arr[i+1][j]=t;
                }
                //END 아래
            }
        }
        return ans;
    }
    static int check(String[][] a) {
        for (int i=0; i<N; i++) {
            cnt = 1;
            for (int j=1; j<N; j++) {
                if (a[i][j].equals(a[i][j-1]) ) {
                    cnt += 1;
                } else {
                    cnt = 1;
                }
                if (ans < cnt) ans = cnt;
            }
            cnt = 1;
            for (int j=1; j<N; j++) {
                if (a[j][i].equals(a[j-1][i])) {
                    cnt += 1;
                } else {
                    cnt = 1;
                }
                if (ans < cnt) ans = cnt;
            }
        }
        return ans;
    }
}

