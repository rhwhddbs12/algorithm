package programmersLV1;

import java.util.Arrays;
import java.util.Scanner;

class day8 {

    public static void main(String[] args) {
        day8 day8 = new day8();
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); //전체 학생 수
        int L = scanner.nextInt(); //잃어버린 학생 수
        int R = scanner.nextInt(); //여벌 체육복을 갖고있는 학생 수

        int lost[] = new int[L];
        int reserve[] = new int[R];

        for (int i=0 ; i< L ; i++){
            lost[i] = scanner.nextInt();
        }
        for (int i=0 ; i< R ; i++){
            reserve[i] = scanner.nextInt();
        }

        System.out.println("Arrays.toString(lost) = " + Arrays.toString(lost));
        System.out.println("Arrays.toString(reserve) = " + Arrays.toString(reserve));
        System.out.println("result: "+day8.solution(N,lost,reserve));

        scanner.close();
        scanner = null;
    }


    public int solution(int n, int[] lost, int[] reserve) {
        int answer = 0;
        answer = n - lost.length;  // 기본적으로 수업에 참여할 수 있는 학생 수

        //1.여벌의 옷을 도난 당한 경우
        for (int i = 0; i < reserve.length; i++) {
            for (int j = 0; j < lost.length; j++) {
                if (reserve[i] == lost[j]) {
                    lost[j] = -100;
                    reserve[i] = -100;
                    answer++;
                    break;
                }
            }
        }
        //2.여벌의 옷이 있는 학생의경우
        for (int i = 0; i < reserve.length; i++) {
            for (int j = 0; j < lost.length; j++) {
                if(reserve[i]!=100 && lost[j]!=100)
                if(Math.abs(reserve[i]-lost[j]) ==1){
                    reserve[i] = -100;
                    lost[j] = -100;
                    answer++;
                    break;
                }
            }

        }

        return answer;
    }
}
