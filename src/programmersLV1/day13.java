package programmersLV1;

import java.util.Scanner;

public class day13 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(solution(a,b));

    }
    public static long solution(int a, int b) {
        long answer = 0;
        if(a!=b){
            if(a>b){
                int temp = 0;
                temp = a;
                a= b;
                b= temp;
            }
            for (int i = a; i <= b; i++) {
                answer+=i;
            }
        }else{
            answer = a;
        }
        return answer;
    }
}
