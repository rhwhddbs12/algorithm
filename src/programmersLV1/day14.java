package programmersLV1;

// 숫자 문자열과 영단어

import java.util.Scanner;
//0~9 -> 48~57
//a~z -> 97~122

/**
 * z zero
 * o one
 * t two three
 * f four five
 * s six seven
 * e eight
 * n nine
 */
public class day14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < str.length(); i++) {
            // 숫자일 때
            if ((int) str.charAt(i) < 58 && (int) str.charAt(i) > 47) {
                sb.append(str.charAt(i));
                continue;
            }
            // zero, nine
            if (str.charAt(i) == 'z'||str.charAt(i)=='n') {
                if (str.charAt(i)=='z')sb.append(0);
                else sb.append(9);
                i+=3;
                continue;
            }
            // eight
            if (str.charAt(i) == 'e') {
                sb.append(8); i+=4;
                continue;
            }

            // one
            if (str.charAt(i) == 'o') {
                sb.append(1); i+=2;
                continue;
            }
            // two, three
            if (str.charAt(i) == 't') {
                if (str.charAt(i + 1) == 'w') {
                    sb.append(2); i+=2;
                }else{
                    sb.append(3); i+=4;
                }
                continue;
            }
            // four, five
            if (str.charAt(i) == 'f') {
                if (str.charAt(i + 1) == 'o') {
                    sb.append(4);
                }else{
                    sb.append(5);
                }
                i+=3;
                continue;
            }
            //seven, six
            if (str.charAt(i) == 's') {
                if (str.charAt(i + 1) == 'i') {
                    sb.append(6); i+=2;
                }else{
                    sb.append(7); i+=4;
                }
                continue;
            }

        }
        int res = Integer.parseInt(sb.toString());
        System.out.println(res);
    }
}
