package programmersLV1;

import java.util.*;

public class day12 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int d = scanner.nextInt();
        int arr[] = new int[N];

        for(int i=0;i<N;i++){
            arr[i]= scanner.nextInt();
        }
        System.out.println(Arrays.toString(solution(arr,d)));
    }


    public static int[] solution(int[] arr, int divisor) {
        int[] answer = {};
        ArrayList<Integer> a = new ArrayList<Integer>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % divisor == 0) {
                a.add(arr[i]);
            }
        }
        if(a.size()==0){
            a.add(-1);
        }
        a.sort(null);
        answer = new int[a.size()];
        for (int i = 0; i < answer.length; i++) {
            answer[i] = a.get(i);
        }

        return answer;
    }

}
