/**
 * 프로그래머스 완주하지 못한 선수 해시 문제
 */
package programmersLV1;

import java.util.HashMap;
import java.util.Map;

public class day4 {
    public static void main(String[] args) {
        String[] participant = {"Leo","kiki","eden","eden"};
        String[] completion = {"eden","kiki","Leo"};
        Sol sol = new Sol();
        sol.solution(participant,completion);
        }
}
class Sol{
    public String solution(String[] participant, String[] completion) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < participant.length; i++) {
            for (int j = 0; j < completion.length; j++) {
                if (participant[i].equals(completion[j]))
                    map.put(participant[i], completion[j]);

            }
        }
        System.out.println("map = " + map);
        String answer = "";
        return answer;
    }
}