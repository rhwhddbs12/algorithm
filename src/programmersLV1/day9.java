package programmersLV1;

import java.util.Scanner;

public class day9 {
    public static void main(String[] args) {
        day9 days = new day9();
        Scanner scanner = new Scanner(System.in);
        int mon = scanner.nextInt();
        int d = scanner.nextInt();


        System.out.println(days.solution(mon,d));

    }

    public String solution(int a, int b) {
        String answer = "";
        String[] day = {"SUN","MON","TUE","WED","THU","FRI","SAT"};
        int[] month ={31,29,31,30,31,30,31,31,30,31,30,31};
        int sum = 0;
        if(a>1) {
            for (int i = 0; i < a-1; i++) {
                sum = sum+month[i] ;

            }
            sum += b;
        }else sum=b;
        System.out.println("sum = " + sum);
        switch (sum % 7) {
            case 1 : answer = day[5]; break;
            case 2 : answer = day[6]; break;
            case 3 : answer = day[0]; break;
            case 4 : answer = day[1]; break;
            case 5 : answer = day[2]; break;
            case 6 : answer = day[3]; break;
            case 0: answer = day[4]; break;
        }

        return answer;
    }
}
