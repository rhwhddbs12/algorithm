package programmersLV1;

import java.util.Arrays;
import java.util.Scanner;

public class test {
    static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] v= new int[3][2];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                v[i][j]= scanner.nextInt();
            }
        }
        System.out.println(Arrays.deepToString(v));
        System.out.println(Arrays.toString(sol(v)));
    }
    public static int[] sol(int[][] v){
        int[] res = new int[2];
        int cnt=0;
        int cnt2=0;
        for (int i = 0; i < 3; i++) {
            if (v[0][0] != v[i][0]) {
                cnt++;
                res[0]=v[i][0];
            }
            if (v[0][1] != v[i][1]) {
                cnt2++;
                res[1]=v[i][1];
            }
        }
        if (cnt == 2) {
            res[0] = v[0][0];
        }
        if (cnt2 == 2) {
            res[1]= v[0][1];
        }
        return res;
    }
}
