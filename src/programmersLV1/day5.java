package programmersLV1;

import java.util.ArrayList;
import java.util.Arrays;

public class day5 {
    public static void main(String[] args) {
        int[] numbers = {2,1,3,4,1};
        ArrayList<Integer> ary = new ArrayList<Integer>();
        for(int i = 0; i <numbers.length-1;i++){
            for(int j = 1; j <numbers.length;j++) {
                if(i==j) continue;
                ary.add(numbers[i]+numbers[j]);
            }
        }
        int temp = 0;
        for(int i = 0; i <ary.size()-1;i++){
            for(int j = 1; j < ary.size();j++){
                    if(i!=j && ary.get(i)==ary.get(j)){
                        ary.remove(j);
                    }
            }
        }
        ary.sort(null);
        int[] result = new int[ary.size()];

        for(int i=0; i< ary.size();i++){
            result[i]=ary.get(i);
        }


        System.out.println("result = " + Arrays.toString(result));
    }
}
