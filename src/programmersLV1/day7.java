package programmersLV1;

import java.util.*;

public class day7 {
    public static void main(String[] args) {
        day7 d = new day7();
        int[] ary ={1,5,2,6,3,7,4};
        int[][] commands = {{2,5,3,},{4,4,1},{1,7,3}};
        System.out.println(Arrays.toString(d.solution(ary,commands)));
    }

    public int[] solution(int[] array, int[][] commands) {
        int[] answer = new int[3];
        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < 3; i++) {
            for (int j = commands[i][0]-1; j < commands[i][1]; j++) {
                list.add(array[j]);
                Collections.sort(list);
            }
            answer[i]=list.get(commands[i][2]-1);
           list.removeAll(list);
        }
        return answer;
    }
}
