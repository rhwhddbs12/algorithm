/**
 * 중복된 원소가 있는 정수 배열이 주어졌을 때, 주요 원소를 찾으시오.
 * 주요 원소는 배열 크기의 반을 초과하여 등장하는 원소입니다.
 *
 * Input: [2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2]
 *
 * Output: 2
 */
package programmersLV1;

public class day2 {
    public static void main(String[] args){
        int [] ary= {2,8,7,2,2,5,2,3,1,2,2};
        int [] num = new int[9];
        int size = ary.length;
        int count = 0;
        for(int i = 1; i<10 ; i ++) {
            count = 0;
            for (int j = 0; j < size; j++) {
                if (ary[j] == i)
                    count++;
                if (j == size - 1) {
                    num[i - 1] = count;
                    if (num[i - 1] > size / 2)
                        System.out.printf("주요원소:%d %n", i);
                }
            }
        }
    }

}
