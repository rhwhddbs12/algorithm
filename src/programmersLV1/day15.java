package programmersLV1;

import java.util.ArrayList;
import java.util.Collections;
// K번째수
class day15 {
    public int[] solution(int[] array, int[][] commands) {
        ArrayList<Integer> ary = new ArrayList();
        int[] answer = new int[commands.length];

        for (int i = 0; i < commands.length; i++) {
            for (int j = commands[i][0]-1; j < commands[i][1]; j++) {
                ary.add(array[j]);
            }
            Collections.sort(ary);
            answer[i]=ary.get(commands[i][2]-1);
            ary.removeAll(ary);
        }
        return answer;
    }
}
