/**
 * day3) 프로그래머스 - 크레인 인형뽑기 게임(2019 카카오 개발자 겨울 인턴쉽)
 */

package programmersLV1;
import java.util.ArrayList;
import java.util.List;

public class day3 {

    public static void main(String[] args){
        int [][] board = {{0,0,0,0,0},{0,0,1,0,3},{0,2,5,0,1},{4,2,4,4,2},{3,5,1,3,1}};
        int [] moves = {1,5,3,5,1,2,1,4};
        Solution solution = new Solution();
        solution.solution(board,moves);
    }}
class Solution {
    public int solution(int[][] board, int[] moves) {
        int answer = 0;
        List<Integer> temp = new ArrayList<Integer>();
        for(int i=0;i<moves.length;i++){
            for(int j =board.length-1;j>=0 ;j--){
                if(board[moves[i]-1][j]==0){
                    continue;
                }else {
                    temp.add(board[moves[i] - 1][j]);
                    board[moves[i] - 1][j]=0;
                    if(temp.size()>1) {
                        if (temp.get(temp.size() - 1) == temp.get(temp.size() - 2)) {
                            temp.remove(temp.size()-1);
                            if(temp.size()==1) {
                                temp.add(0, -1);
                                temp.remove(temp.size() - 1);
                                answer += 2;
                            }else {
                                temp.remove(temp.size() - 1);
                                answer += 2;
                            }
                        }
                    }
                    break;
                }
            }
        }
        System.out.println("answer = " + answer);
        return answer;
    }
}