package programmersLV1;

import java.util.Scanner;

public class day10 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String getMsg = scanner.nextLine();
        day10 day10 = new day10();
        System.out.println(day10.solution(getMsg));

    }



    public String solution(String s) {
        String answer = "";

        int a = s.length()/2;
        if(s.length()%2==0){

            answer = s.substring(a-1,a+1);
        }else{
            answer = Character.toString(s.charAt(a));
        }
        return answer;

    }
}
