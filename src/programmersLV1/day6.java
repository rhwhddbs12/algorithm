package programmersLV1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class day6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        int[] res = new int[5];
        int[] ap = {1, 2, 3, 4, 5};
        int[] bp = {2, 1, 2, 3, 2, 4, 2, 5};
        int[] cp = {3, 3, 1, 1, 2, 2, 4, 4, 5, 5};

        for (int i = 0; i < res.length; i++) {
            res[i] = scanner.nextInt();
        }
        int a = solution(res,ap,T);
        int b = solution(res,bp,T);
        int c = solution(res,cp,T);
        System.out.println(Arrays.toString(sol(a,b,c)));
    }

    public static int solution(int[] res, int[] req, int T) {
        int[] answer;
        int d = req.length;
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < T; i++) {
            if (res[i%5] == req[i%d]) {
                list.add(res[i%5]);
            }
        }
        return list.size();
    }
    public static int[] sol(int a, int b, int c ) {
        List<Integer> list = new ArrayList<Integer>();

        if(a>b && a>c){
            list.add(1);
        }else if (b>a && b>c){
            list.add(2);
        }else if(c>a && c>b){
            list.add(3);
        }else{
            if(a==b && a==c) {
                list.add(1);
                list.add(2);
                list.add(3);
            }
            else if(a==b){
                list.add(1);
                list.add(2);
            }else if(a==c){
                list.add(1);
                list.add(3);
            }else if(b==c){
                list.add(2);
                list.add(3);
            }
        }
        int f[] = new int[list.size()];
        for(int i =0; i<f.length;i++) {
            f[i] = list.get(i);
        }
        return f;
    }
}
