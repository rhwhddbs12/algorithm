package programmersLV1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class day11 {

    public static void main(String[] args) {
        day11 day11 = new day11();
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] input = new int[N];

        for (int i = 0; i < N; i++) {
            input[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(day11.solution(input)));
    }

    public int[] solution(int []arr) {
        int[] answer = {};
        List a = new ArrayList();


        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                a.add(arr[i]);
            }else if(arr[i]!=arr[i-1]){
                a.add(arr[i]);
            }

        }
        answer = new int[a.size()];
        for (int i = 0; i < answer.length; i++) {
            answer[i]=(int)a.get(i);
        }
        return answer;
    }
}
