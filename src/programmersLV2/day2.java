package programmersLV2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class day2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();
        int[] progresses = new int[p];
        int[] speeds = new int[p];
        for(int i=0; i < p; i ++){
            progresses[i]= scanner.nextInt();
        }
        for(int i=0; i < p; i ++){
            speeds[i]=scanner.nextInt();
        }
        System.out.println(Arrays.toString(solution(progresses,speeds)));
    }
    public static int[] solution(int[] progresses, int[] speeds) {

        List<Integer> p = new ArrayList<Integer>();
        List<Integer> a = new ArrayList<Integer>();
        for (int i = 0; i < progresses.length; i++) {
            if ((100 - progresses[i]) % speeds[i] > 0) {
                p.add( (100 - progresses[i]) / speeds[i]+1 );
            }else{
                p.add( (100 - progresses[i]) / speeds[i] );
            }
        }
        int an = 1;
        for(int i=0; i<p.size(); i++){
            if (p.get(i) < 0) {
                continue;
            }
            if (i < p.size() - 1) {
                for (int j = i + 1; j < p.size(); j++) {

                    if (p.get(i) >= p.get(j)) {
                        an++;
                        p.set(j, -1);
                        if(j != p.size()-1) continue;
                        else{
                            a.add(an);
                            an = 1;
                        }

                    } else {
                        a.add(an);
                        an = 1;

                    }

                    break ;
                }
            }else{
                a.add(an);
            }
        }
        int[] answer = new int[a.size()];
        for(int i=0;i<a.size();i++){
            answer[i]=a.get(i);
        }

        return answer;

    }
}
