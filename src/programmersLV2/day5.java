package programmersLV2;

import java.util.*;

public class day5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] numbers = new int[size];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextInt();
        }

        System.out.println(solution(numbers));
    }
        public static String solution(int[] numbers) {
            String answer = "";
            String[] str = new String[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                str[i] = (String.valueOf(numbers[i]));
            }
            System.out.println("str = " + Arrays.toString(str));
            Arrays.sort(str, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return (s2+s1).compareTo(s1+s2);
                }
            });
            for (int i = 0; i < str.length; i++) {
                answer+=str[i];
            }
            if (str[0].equals("0")) {
                answer = "0";
            }
            return answer;
        }


}
