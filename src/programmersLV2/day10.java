package programmersLV2;

import java.util.Scanner;

//타겟넘버
public class day10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(); // 2<=N<=20
        int numbers[] = new int[N];
        for (int i = 0; i < N; i++) {
            numbers[i]= scanner.nextInt();
        }
        int target = scanner.nextInt();

        solution(numbers, target);

    }

    public static int solution(int[] numbers, int target) {
        int sum=0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return 1;
    }
}
