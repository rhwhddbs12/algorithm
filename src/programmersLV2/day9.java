package programmersLV2;

import java.util.Arrays;
import java.util.Scanner;

//124 나라의 숫자
public class day9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(solution(n));

    }

    public static String solution(int n) {
        StringBuffer sb = new StringBuffer();
        String res ="";
        int[] d = {4,1,2};

        while (n > 0) {
            int rem = n%3;

            sb.append(d[rem]);
            n/=3;
            if(rem==0) n-=1;

        }
        sb.reverse();
        res=sb.toString();
        return res;
    }
}
