package programmersLV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class day3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int bl = scanner.nextInt();
        int weight = scanner.nextInt();
        int tnum = scanner.nextInt();
        int[] tweight = new int[tnum];

        for (int i = 0; i < tnum; i++) {
            tweight[i] = scanner.nextInt();
        }
        System.out.println(solution(bl,weight,tweight));
    }

    public static int solution(int bridge_length, int weight, int[] truck_weights) {
        List<Integer> time = new ArrayList<Integer>();
        int answer=0;
        for (int i = 0; i < truck_weights.length; i++) {
            time.add(truck_weights[i]);
            weight -= truck_weights[i];
            answer++;
            for (int j = 0; j< bridge_length ; j++) {
                if(time.size() == bridge_length){
                    weight+= time.get(0);
                    time.remove(0);
                }
                if (i < truck_weights.length-1 && weight - truck_weights[i+1] >= 0) {
                        break;
                } else {
                    time.add(0);
                    answer++;
                }
            }
        }
        return answer;
    }
}