package programmersLV2;

import java.util.Arrays;
import java.util.Scanner;

public class day6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] citations = new int[size];
        for (int i = 0; i < size; i++) {
            citations[i] = scanner.nextInt();
        }
        System.out.println(solution(citations));
    }
    public static int solution(int[] citations) {
        int answer = 0;
        int count = 0;
        System.out.println("citations = " + Arrays.toString(citations));
        Arrays.sort(citations);
        System.out.println("citations sort = " + Arrays.toString(citations));

        for (int i = citations.length-1; i >=0 ; i--) {
            for (int j = citations.length-1; j >= 0; j--) {
                if (citations[i] <= citations[j]) {
                    count++;
                }

                if (citations[i] >= count && count >= answer) {
                    answer = count;
                }

                if (citations[i] <= count && citations[i] != 0 && citations[i] > answer) {
                    answer = citations[i];
                    System.out.println("answer1 = " + answer);

                }
            }
            System.out.println("answer2 = " + answer);
            System.out.println("citations["+i+"]="+citations[i]+" 총:"+count);
            count=0;

        }
        return answer;
    }
}

