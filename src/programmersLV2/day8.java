package programmersLV2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
// 가로는 세로길이보다 같거나 길다.
public class day8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int b = scanner.nextInt();
        int y = scanner.nextInt();

        System.out.println(solution(b,y));
    }
    public static List<Integer> solution(int brown, int yellow) {
        List<Integer> answer = new ArrayList<>();
        List<Integer> list = new ArrayList<>();

        //전체 정사각형의 약수
        for (int i = 1; i <= brown+yellow; i++) {
            if ((brown+yellow) % i == 0) {
                list.add(i);

            }
        }
        for (int i = 0; i <= list.size()/2; i++) {
            if (list.get(i) > 2) {
                if ((list.get(list.size() - 1 - i) - 2) * (list.get(i)-2) == yellow) {
                    answer.add((list.get(list.size() - 1 - i))); //가로
                    answer.add(list.get(i)); //세로
                    break;
                }
            }
        }
        System.out.println("list = " + list);

       return answer;
    }
}
