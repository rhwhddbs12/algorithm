package programmersLV2;

import java.util.*;

public class day1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String skill = scanner.next();

        int N = scanner.nextInt();
        String[] skill_trees = new String[N];
        for(int i=0; i<N;i++){
            skill_trees[i]= scanner.next();

        }
        System.out.println(solution(skill,skill_trees));
    }
    public static int solution(String skill, String[] skill_trees) {
        int answer = 0;
        int b;
        List<Integer> a ;
        for (int i = 0; i < skill_trees.length; i++) {
            a = new ArrayList<Integer>();
            b=0;
            p:for (int j = 0; j < skill_trees[i].length(); j++) {
                for (int x = 0; x < skill.length(); x++) {
                    if (skill_trees[i].charAt(j) == skill.charAt(x)) {
                        a.add(x);
                        break ;
                    }
                }
                if(a.size()>0) {
                    if(a.get(0)!=0) {
                        b = 1;
                        break p;
                    }
                }
                if(a.size()>1){
                    if (a.get(a.size() - 1) - a.get(a.size() - 2) != 1) {
                        b=1;
                        break p;
                    }
                }
            }
            if (b != 1) {
                answer += 1;
            }
        }
        return answer;
    }
}